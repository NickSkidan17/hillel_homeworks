package hw17.processor;

import hw17.entities.*;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Helper {
    private Connection connect;

    public Helper() throws SQLException {
        connect = Connector.getConnection();
    }

    public void getResultForSelectingFilms(String string) {
        Statement st = null;
        try {
            st = getStatement();
            ResultSet rs;
            try {
                rs = st.executeQuery(string);
                ArrayList<Film> listFilms = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String title = rs.getString(2);
                    Date releaseYear = rs.getDate(3);
                    String releaseCountry = rs.getString(4);
                    listFilms.add(new Film(id, title, releaseYear, releaseCountry));
                }
                if (listFilms.size() > 0) {
                    System.out.println(listFilms);
                } else {
                    System.out.println("Not found");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            closeStatement(st);
        }
    }

    public void getResultForDeletingFilms(String string) {
        Statement st = null;
        try {
            st = getStatement();
            try {
                int deletedRows = st.executeUpdate(string);
                if (deletedRows > 0) {
                    System.out.println(deletedRows + " film(s) was(were) deleted from cinema");
                } else {
                    System.out.println("Nothing was deleted");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            closeStatement(st);
        }
    }

    public void getResultForSelectingActors(String string) {
        Statement st = null;
        try {
            st = getStatement();
            ResultSet rs;
            try {
                rs = st.executeQuery(string);
                ArrayList<Actor> listActors = new ArrayList<>();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    Date birthdate = rs.getDate(3);
                    listActors.add(new Actor(id, name, birthdate));
                }
                if (listActors.size() > 0) {
                    System.out.println(listActors);
                } else {
                    System.out.println("Not found");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            closeStatement(st);
        }
    }

    private Statement getStatement() {
        Statement st = null;
        try {
            st = connect.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return st;
    }

    private void closeStatement(Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
