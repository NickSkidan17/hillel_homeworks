package hw17.entities;

import java.util.Date;

public class Film {
    private int id;
    private String title;
    private Date releaseYear;
    private String releaseCountry;

    public Film(int id, String title, Date releaseYear, String releaseCountry) {
        this.id = id;
        this.title = title;
        this.releaseYear = releaseYear;
        this.releaseCountry = releaseCountry;
    }

    @Override
    public String toString() {
        return "Film [id: " + id + "; title: " + title + "; year of release: "
                + releaseYear + "; release country: " + releaseCountry + "]";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Date releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getReleaseCountry() {
        return releaseCountry;
    }

    public void setReleaseCountry(String releaseCountry) {
        this.releaseCountry = releaseCountry;
    }
}
