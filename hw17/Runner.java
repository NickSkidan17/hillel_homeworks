/*
Видеотека. В БД хранится информация о домашней видеотеке: фильмы, актеры, режиссеры.
Для фильмов необходимо хранить:
— название;
— имена актеров;
— имена режисеров;
— дату выхода;
— страну, в которой выпущен фильм.
Для актеров и режиссеров необходимо хранить:
— ФИО;
— дату рождения.
• Найти все фильмы, вышедшие на экран в текущем и прошлом году.
• Вывести информацию об актерах, снимавшихся в заданном фильме.
• Вывести информацию об актерах, снимавшихся как минимум в N фильмах.
• Вывести информацию об актерах, которые были режиссерами хотя бы одного из фильмов.
• Удалить все фильмы, дата выхода которых была более заданного числа лет назад.
 */

package hw17;

import hw17.processor.*;

import java.sql.SQLException;

public class Runner {
    private final static String SELECT_FILM = "select * from film where YEAR(release_year) >= 2018;";
    private final static String SELECT_ACTOR_FROM_FILM =
            "select * from actor where idactor in (select actor_id from film_actors where filma_id = 9)";
    private final static String SELECT_ACTOR_DIRECTOR =
            "select a.idactor, a.name, a.birthdate from actor a inner join director d on a.name = d.name";
    private final static String SELECT_ACTOR_IN_MORE_THAN_ONE_FILM =
            "select a.idactor, a.name, a.birthdate from actor a \n" +
                    "inner join film_actors fa on a.idactor = fa.actor_id \n" +
                    "group by fa.actor_id having count(filma_id) >= 2;";
    private final static String DELETE_FILMS_RELEASED_MORE_THAN_FIFTEEN_YEARS =
            "delete from film where release_year <= subdate(curdate(), interval 40 YEAR)";

    public static void main(String[] args) {
        Helper helper;
        try {
            helper = new Helper();
            System.out.println("Films from cinema from 2018 to 2019: ");
            helper.getResultForSelectingFilms(SELECT_FILM);
            System.out.println();
            System.out.println("Actor from Godfather is: ");
            helper.getResultForSelectingActors(SELECT_ACTOR_FROM_FILM);
            System.out.println();
            System.out.println("Actors, who were directors at the same time in their films: ");
            helper.getResultForSelectingActors(SELECT_ACTOR_DIRECTOR);
            System.out.println();
            System.out.println("Actors, who played in more than one film from my cinema: ");
            helper.getResultForSelectingActors(SELECT_ACTOR_IN_MORE_THAN_ONE_FILM);
            System.out.println();
            System.out.println("Deleting films, which were released more than 15 years ago: ");
            helper.getResultForDeletingFilms(DELETE_FILMS_RELEASED_MORE_THAN_FIFTEEN_YEARS);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}