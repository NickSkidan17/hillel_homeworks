/*
Порт. Корабли заходят в порт для разгрузки/загрузки контейнеров. Число
контейнеров, находящихся в текущий момент в порту и на корабле, должно
быть неотрицательным и превышающим заданную грузоподъемность судна и
вместимость порта. В порту работает несколько причалов. У одного
причала может стоять один корабль. Корабль может загружаться у причала,
разгружаться или выполнять оба действия
 */

package hw11;

import hw11.entities.*;

public class Runner {
    public static void main(String[] args) {
        Port port = new Port(100);
        Thread[] ships = new Thread[4];

        for (int i = 0; i < ships.length; i++) {
            ships[i] = new Ship("Ship " + i, 50, port);
        }

        for (Thread thread : ships) {
            thread.start();
        }
    }
}
