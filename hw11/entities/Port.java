package hw11.entities;

import java.util.Random;

public class Port {
    private int currentContainersNumber;
    private int maxContainersNumber;
    private int unloadedPortContainersCount = 0;
    private int loadedPortContainersCount = 0;

    private Random random;

    public Port(int maxContainersNumber) {
        this.maxContainersNumber = maxContainersNumber;
        random = new Random();
        this.currentContainersNumber = random.nextInt(maxContainersNumber);
        System.out.println(this);
    }

    public void executeShipAction(Ship ship) {

        System.out.println(ship + " " + this + "\n");

        int currentAction = random.nextInt(3);
        String action = "";
        switch (currentAction) {
            case 0:
                action = "loaded";
                actionIsLoadShip(ship);
                break;
            case 1:
                action = "unloaded";
                actionIsUnLoadShip(ship);
                break;
            case 2:
                action = "loaded and unloaded";
                actionIsLoadShip(ship);
                actionIsUnLoadShip(ship);
                break;
        }
        try {
            Ship.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(ship.getName() + " " + action + "(loaded: " + unloadedPortContainersCount + "; unloaded: " +
                loadedPortContainersCount + ")" + "\n" + ship + " " + this + "\n");

    }

    private synchronized void loadShip(Ship ship, int containersCount) {
        while (!portCanBeUnloaded(containersCount)) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
        ship.load(containersCount);
        unloadPort(containersCount);
        notifyAll();
    }

    private synchronized void unloadShip(Ship ship, int containersCount) {
        while (!portCanBeLoaded(containersCount)) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("Interrupted exception while unloading ship");
            }
        }
        ship.unload(containersCount);
        loadPort(containersCount);
        notifyAll();
    }

    private void actionIsLoadShip(Ship ship) {
        if (currentContainersNumber == 0) {
            System.out.println("Ship " + ship.getName() + " can not be loaded. Port is empty.");
            loadShip(ship, 0);
        } else {
            loadedPortContainersCount = obtainShipContainersCountForShipLoading(ship);
            if (loadedPortContainersCount > currentContainersNumber) {
                System.out.println("Ship " + ship.getName() + " can be loaded only " + currentContainersNumber);
                loadShip(ship, currentContainersNumber);
            } else {
                loadShip(ship, loadedPortContainersCount);
            }
        }
    }

    private void actionIsUnLoadShip(Ship ship) {
        int availableContainersForShipUnloaded = maxContainersNumber - currentContainersNumber;
        if (availableContainersForShipUnloaded == 0) {
            System.out.println("Ship " + ship.getName() + " can not be unloaded. Port is full.");
            unloadShip(ship, 0);
        } else {
            unloadedPortContainersCount = obtainShipContainersCountForShipUnloading(ship);
            if (unloadedPortContainersCount + currentContainersNumber > maxContainersNumber) {
                System.out.println("Ship " + ship.getName() + " can be unloaded only " + availableContainersForShipUnloaded);
                unloadShip(ship, availableContainersForShipUnloaded);
            } else {
                unloadShip(ship, unloadedPortContainersCount);
            }
        }
    }

    private void loadPort(int containersCount) {
        this.currentContainersNumber += containersCount;
    }

    private void unloadPort(int containersCount) {
        this.currentContainersNumber -= containersCount;
    }

    private boolean portCanBeUnloaded(int containersCount) {
        return containersCount <= currentContainersNumber;
    }

    private boolean portCanBeLoaded(int containersCount) {
        return containersCount <= availableContainersCountForLoading();
    }

    private int obtainShipContainersCountForShipUnloading(Ship ship) {
        return random.nextInt(ship.getCurrentContainersNumber());
    }

    private int obtainShipContainersCountForShipLoading(Ship ship) {
        return random.nextInt(ship.getMaxContainersNumber() - ship.getCurrentContainersNumber());
    }

    private int availableContainersCountForLoading() {
        return maxContainersNumber - currentContainersNumber;
    }

    @Override
    public String toString() {
        return "Port: [max number of containers: " + maxContainersNumber + ", current number of containers: "
                + currentContainersNumber + "]";
    }

    public int getMaxContainersNumber() {
        return maxContainersNumber;
    }

    public void setMaxContainersNumber(int maxContainersNumber) {
        this.maxContainersNumber = maxContainersNumber;
    }

    public int getCurrentContainersNumber() {
        return currentContainersNumber;
    }

    public void setCurrentContainersNumber(int currentContainersNumber) {
        this.currentContainersNumber = currentContainersNumber;
    }

    public int getUnloadedPortContainersCount() {
        return unloadedPortContainersCount;
    }

    public void setUnloadedPortContainersCount(int unloadedPortContainersCount) {
        this.unloadedPortContainersCount = unloadedPortContainersCount;
    }

    public int getLoadedPortContainersCount() {
        return loadedPortContainersCount;
    }

    public void setLoadedPortContainersCount(int loadedPortContainersCount) {
        this.loadedPortContainersCount = loadedPortContainersCount;
    }
}
