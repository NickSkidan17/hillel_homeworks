package hw11.entities;


import java.util.Random;

public class Ship extends Thread {
    private int currentContainersNumber;
    private int maxContainersNumber;
    private Port port;

    private Random random;

    public Ship(String name, int maxContainersNumber, Port port) {
        super(name);
        this.maxContainersNumber = maxContainersNumber;
        this.port = port;
        random = new Random();
        currentContainersNumber = random.nextInt(maxContainersNumber);
    }

    @Override
    public void run() {
        while (true) {
            port.executeShipAction(this);
        }
    }

    public void load(int containersCount) {
        this.currentContainersNumber += containersCount;
    }

    public void unload(int containersCount) {
        this.currentContainersNumber -= containersCount;
    }

    @Override
    public String toString() {
        return this.getName() + ": [max number of containers: " + maxContainersNumber + ", current number of containers: " + currentContainersNumber + "]";
    }

    public int getMaxContainersNumber() {
        return maxContainersNumber;
    }

    public void setMaxContainersNumber(int maxContainersNumber) {
        this.maxContainersNumber = maxContainersNumber;
    }

    public int getCurrentContainersNumber() {
        return currentContainersNumber;
    }

    public void setCurrentContainersNumber(int currentContainersNumber) {
        this.currentContainersNumber = currentContainersNumber;
    }

    public Port getPort() {
        return port;
    }

    public void setPort(Port port) {
        this.port = port;
    }

}
