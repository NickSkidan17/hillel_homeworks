package hw6;

public class Car {
    private String mark;
    private String model;
    private int topSpeed;
    private double fuelConsumption;
    private int price;

    Car(String mark, String model, int topSpeed, double fuelConsumption, int price) {
        this.mark = mark;
        this.model = model;
        this.topSpeed = topSpeed;
        this.fuelConsumption = fuelConsumption;
        this.price = price;
    }

    @Override
    public String toString() {
        return "mark: " + mark + "; model: " + model + "; topSpeed: " + topSpeed + " km/h; fuelConsumption: " + fuelConsumption + " liters/100km; price: " + price;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
