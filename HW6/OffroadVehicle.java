package hw6;

public class OffroadVehicle extends Car {
    private String driveType;
    private double trunkVolume;

    OffroadVehicle(String mark, String model, int topSpeed, int fuelConsumption, int price, String driveType, double trunkVolume) {
        super(mark, model, topSpeed, fuelConsumption, price);
        this.driveType = driveType;
        this.trunkVolume = trunkVolume;
    }

    @Override
    public String toString() {
        return super.toString() + "; driveType: " + driveType + "; trunkVolume: " + trunkVolume + " liters.";
    }

    public String getDriveType() {
        return driveType;
    }

    public void setDriveType(String driveType) {
        this.driveType = driveType;
    }

    public double getTrunkVolume() {
        return trunkVolume;
    }

    public void setTrunkVolume(double trunkVolume) {
        this.trunkVolume = trunkVolume;
    }
}
