package hw6;

public class Minivan extends PassengerVehicle {
    private int passengerSeatsNumber;

    Minivan(String mark, String model, int topSpeed, int fuelConsumption, int price, String fuelType, String transmissionType, int passengerSeatsNumber) {
        super(mark, model, topSpeed, fuelConsumption, price, fuelType, transmissionType);
        this.passengerSeatsNumber = passengerSeatsNumber;
    }

    public String toString() {
        return super.toString() + "; passengerSeatsNumber: " + passengerSeatsNumber;
    }

    public int getPassengerSeatsNumber() {
        return passengerSeatsNumber;
    }

    public void setPassengerSeatsNumber(int passengerSeatsNumber) {
        this.passengerSeatsNumber = passengerSeatsNumber;
    }
}
