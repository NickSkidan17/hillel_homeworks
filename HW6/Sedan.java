package hw6;

public class Sedan extends PassengerVehicle {
    private String carRoofType;

    Sedan(String mark, String model, int topSpeed, int fuelConsumption, int price, String fuelType, String transmissionType, String carRoofType) {
        super(mark, model, topSpeed, fuelConsumption, price, fuelType, transmissionType);
        this.carRoofType = carRoofType;
    }

    @Override
    public String toString() {
        return super.toString() + "; carRoofType: " + carRoofType;
    }

    public String getCarRoofType() {
        return carRoofType;
    }

    public void setCarRoofType(String carRoofType) {
        this.carRoofType = carRoofType;
    }
}
