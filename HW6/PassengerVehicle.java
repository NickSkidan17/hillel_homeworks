package hw6;

public class PassengerVehicle extends Car {
    private String fuelType;
    private String transmissionType;

    PassengerVehicle(String mark, String model, int topSpeed, int fuelConsumption, int price, String fuelType, String transmissionType) {
        super(mark, model, topSpeed, fuelConsumption, price);
        this.fuelType = fuelType;
        this.transmissionType = transmissionType;
    }

    @Override
    public String toString() {
        return super.toString() + "; fuelType: " + fuelType + "; transmissionType: " + transmissionType;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(String transmissionType) {
        this.transmissionType = transmissionType;
    }
}
