package hw6;

public class CargoVehicle extends Car {
    private double tonnage;
    private String wheelScheme;

    CargoVehicle(String mark, String model, int topSpeed, int fuelConsumption, int price, double tonnage, String wheelScheme) {
        super(mark, model, topSpeed, fuelConsumption, price);
        this.tonnage = tonnage;
        this.wheelScheme = wheelScheme;
    }

    @Override
    public String toString() {
        return super.toString() + "; tonnage: " + tonnage + " kg; wheelScheme: " + wheelScheme;
    }

    public double getTonnage() {
        return tonnage;
    }

    public void setTonnage(int tonnage) {
        this.tonnage = tonnage;
    }

    public String getWheelScheme() {
        return wheelScheme;
    }

    public void setWheelScheme(String wheelScheme) {
        this.wheelScheme = wheelScheme;
    }
}
