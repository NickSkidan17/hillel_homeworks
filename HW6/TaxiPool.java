package hw6;

public class TaxiPool {
    private Car[] cars;

    TaxiPool(Car[] cars) {
        this.cars = cars;
    }

    public int getTaxiPoolPrice() {
        int taxiPoolPrice = 0;
        for (Car car : cars) {
            taxiPoolPrice += car.getPrice();
        }
        return taxiPoolPrice;
    }

    public Car[] sortTaxiPoolByFuelConsumption() {
        for (int i = 0; i < cars.length; i++) {
            for (int j = i + 1; j < cars.length; j++) {
                if (cars[j].getFuelConsumption() < cars[i].getFuelConsumption()) {
                    Car var = cars[i];
                    cars[i] = cars[j];
                    cars[j] = var;
                }
            }
        }
        return cars;
    }

    public Car[] selectCarsInSpeedInterval(int minSpeed, int maxSpeed) {
        int counter = 0;
        Car[] carsInSpeedInterval = new Car[0];
        for (Car car : cars) {
            if (car.getTopSpeed() >= minSpeed && car.getTopSpeed() <= maxSpeed) {
                Car[] someArray = new Car[carsInSpeedInterval.length + 1];
                for (int i = 0; i < carsInSpeedInterval.length; i++) {
                    someArray[i] = carsInSpeedInterval[i];
                }
                carsInSpeedInterval = someArray;
                someArray[counter] = car;
                counter++;
            }
        }
        return carsInSpeedInterval;
    }

    public Car[] getCars() {
        return cars;
    }

    public void setCars(Car[] cars) {
        this.cars = cars;
    }
}
