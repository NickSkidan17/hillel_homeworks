package hw6;

public class Hatchback extends PassengerVehicle {
    private int doorsNumber;

    Hatchback(String mark, String model, int topSpeed, int fuelConsumption, int price, String fuelType, String transmissionType, int doorsNumber) {
        super(mark, model, topSpeed, fuelConsumption, price, fuelType, transmissionType);
        this.doorsNumber = doorsNumber;
    }

    @Override
    public String toString() {
        return super.toString() + "; doorsNumber: " + doorsNumber;
    }

    public int getDoorsNumber() {
        return doorsNumber;
    }

    public void setDoorsNumber(int doorsNumber) {
        this.doorsNumber = doorsNumber;
    }
}
