/*
Таксопарк. Определить иерархию легковых автомобилей. Создать таксопарк.
Подсчитать стоимость автопарка. Провести сортировку автомобилей парка по расходу топлива.
Найти автомобили в компании, соответствующие заданному диапазону параметров скорости.
Для каждого класса легковых автомобилей определить toString() и использовать его для печати объекта.

- массив, который будет возвращен методом поиска автомобилей по скорости расширять
  динамически(не считать сразу кол-во подходящих под критерий автомобилей);
- в метод по поиску машин по диапазону задавать min и max скорость и проверять,
  входит ли максимальная скорость текущего автомобиля в диапазон;
- в классе таксопарка ничего не печатать!!!(System.out.println())
 */

package hw6;

public class TestTaxiPool {
    public static void main(String[] args) {
        Car[] cars = new Car[10];
        cars[0] = new CargoVehicle("MAN", "TGS", 160, 25, 95000, 90000, "6*6");
        cars[1] = new CargoVehicle("Foton Auman", "H5", 195, 30, 80000, 50000, "4*2");
        cars[2] = new OffroadVehicle("BMW", "X5", 240, 20, 75000, "AWD", 1000);
        cars[3] = new OffroadVehicle("Ford", "Expedition", 220, 27, 70000, "AWD", 500);
        cars[4] = new OffroadVehicle("Cadillac", "Escalade", 250, 33, 120000, "AWD", 1500);
        cars[5] = new Minivan("Peugeot", "Rifter", 200, 17, 40000, "petrol", "manual", 7);
        cars[6] = new Minivan("Toyota", "Alphard", 190, 15, 35000, "diesel", "manual", 9);
        cars[7] = new Hatchback("Volkswagen", "Golf", 280, 13, 60000, "petrol", "automatic", 5);
        cars[8] = new Sedan("VAZ", "2101", 120, 10, 1500, "methane", "manual", "not cabriolet");
        cars[9] = new Sedan("Bentley", "Continental", 300, 24, 200000, "petrol", "semi-automatic", "cabriolet");

        TaxiPool taxiPool = new TaxiPool(cars);

        System.out.println("Total cost of taxi pool is: " + taxiPool.getTaxiPoolPrice() + "$.\n");
        System.out.println("Taxi pool after sorting by fuel consumption: ");
        for (Car car : taxiPool.sortTaxiPoolByFuelConsumption()) {
            System.out.println(car);
        }
        System.out.println();
        System.out.println("Cars in a given speed interval: ");
        for (Car car : taxiPool.selectCarsInSpeedInterval(150, 200)) {
            System.out.println(car);
        }
    }
}
