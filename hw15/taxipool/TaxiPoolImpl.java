package hw15.taxipool;

import hw15.cars.Car;

import java.util.ArrayList;
import java.util.Comparator;

public class TaxiPoolImpl implements TaxiPool {
    private ArrayList<Car> cars;

    public TaxiPoolImpl(ArrayList<Car> cars) {
        this.cars = cars;
    }

    @Override
    public void sortTaxiPoolByFuelConsumption() {
        cars.stream().sorted(Comparator.comparing(Car::getFuelConsumption)).forEach(car -> System.out.println(car));
    }

    @Override
    public int getTaxiPoolPrice() {
        return cars.stream().reduce(0, (sum, car) -> sum += car.getPrice(), (sum1, sum2) -> sum1 + sum2);
    }

    @Override
    public void selectCarsInSpeedInterval(int minSpeed, int maxSpeed) {
        cars.stream().filter(car -> car.getTopSpeed() >= minSpeed && car.getTopSpeed() <= maxSpeed).forEach(car -> System.out.println(car));
    }


    public ArrayList<Car> getCars() {
        return cars;
    }

    public void setCars(ArrayList<Car> cars) {
        this.cars = cars;
    }
}
