package hw15.taxipool;

import hw15.cars.Car;

import java.util.ArrayList;
import java.util.stream.Stream;

public interface TaxiPool {

    int getTaxiPoolPrice();

    void sortTaxiPoolByFuelConsumption();

    void selectCarsInSpeedInterval(int minSpeed, int maxSpeed);

}
