/*
Объявить массив с n-м количеством случайных чисел (явно инициализировать массив)
и вывести числа массива один раз с переходом(столбец) и один раз без перехода на новую строку(строка)
*/

package hw1;

public class Task3 {
    public static void main(String[] args) {
        int[] nums = {11, 22, 56, 765, 27, 135};
        for (int i = 0; i < nums.length; i++) {
            System.out.println(nums[i]);
        }

        System.out.println();

        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i] + " ");
        }
    }
}
