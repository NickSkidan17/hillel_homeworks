//Поприветствовать любого пользователя при вводе его имени через аргумент командной строки

package hw1;

public class Task1 {
    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++) {
            System.out.println("Hello " + args[i] + "!");
        }
    }
}
