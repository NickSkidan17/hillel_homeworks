<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>

<meta charset="UTF-8">

<title>Films released in given period</title>
</head>


	<c:import url="../jspf/head.jspf"></c:import>
	<body>
	<center>
		<h5>Input period : </h5>
		<form name="form1" method="post" action="http://localhost:8080/filmsReleasedInPeriod">
		<table border="0"><tr><td align="top">
		Year from:</td> <td valign="top">
		<input type="number" name="yearFrom" size="4" min="1900" max="2050" step="1">
		</td></tr>
		<tr><td valign="top">
		To year:</td> <td valign="top">
		<input type="number" name="yearTo" size="4" min="1900" max="2050" step="1">
		</td></tr>
		<tr><td valign="top">
		<input type="submit" value="Submit"></td></tr>
		</table></form>
		</center>
	</body>
</html>