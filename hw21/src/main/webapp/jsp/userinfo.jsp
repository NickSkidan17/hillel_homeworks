<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>

<meta charset="UTF-8">

<title>User Info</title>
</head>

	<c:import url="../jspf/head.jspf"></c:import>
	<body>
	<center>
		<h5>User : </h5>
		<tr>
          	Hello, <font size="+2" color="navy"><td>${user.login}</font>!</td>
          	<br />
          	Role: <font size="+1" color="lime"><td>${user.role}</font></td>
        </tr>
		</center>
	</body>
</html>