<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
	<c:import url="../jspf/head.jspf"></c:import>
	<body>
	<center>
		<h5>Films : </h5>
		<c:forEach var="listFilms" items="${listFilms}">
			<tr>
			<font size="+2" color="indigo"<td>${listFilms.title};</td></font>
			<td>${listFilms.releaseYear};</td>
			<td>${listFilms.releaseCountry};</td>
			<font color="purple"><td>Actors: ${listFilms.actors}</td></font>
			<font color="magenta"><td>Director: ${listFilms.directors}</td></font>
			<br />
			</tr>
		</c:forEach>
		</center>
	</body>
</html>