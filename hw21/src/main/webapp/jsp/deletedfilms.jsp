<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
	<c:import url="../jspf/head.jspf"></c:import>
	<body>
	<center>
		<h5>Number of deleted films : </h5>
		<tr>
        	<font size="+2" color="red"><td>${deletedRows}</font> film(s) was(were) deleted</td>
        </tr>
		</center>
	</body>
</html>