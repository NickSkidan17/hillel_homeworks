USE `cinema`;


DROP TABLE `actor`;

DROP TABLE `director`;

DROP TABLE `film`;

DROP TABLE `film_actors`;

DROP TABLE `film_directors`;

DROP TABLE `user`;
