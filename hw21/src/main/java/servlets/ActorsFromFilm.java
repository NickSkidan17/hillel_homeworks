package servlets;

import entities.*;
import utils.Processor;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/actorsFromFilm")
public class ActorsFromFilm extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("jsp/actorsfromfilm.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<Actor> listActors = new ArrayList<>();
        String parameter = request.getParameter("filmTitle");
        try {
            Processor processor = new Processor();
            listActors = processor.getActorsFromFilm(parameter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(listActors == null){
            String errorMessage = "There is no such film in cinema";
            request.setAttribute("errorMessage", errorMessage);
            request.getRequestDispatcher("jsp/actorsfromfilm.jsp").forward(request, response);
        }
        request.setAttribute("listActors", listActors);
        request.getRequestDispatcher("jsp/actorslist.jsp").forward(request, response);
    }
}