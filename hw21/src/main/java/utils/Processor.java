package utils;

import entities.*;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Processor {
    private Helper helper;

    public Processor() throws Exception {
        helper = new Helper();
    }

    public ArrayList<Actor> getActorsFromFilm(String filmTitle) {
        return helper.getResultForSelectingActors("select * from actor where actor.id in \n" +
                "(select idactor from film_actors \n" +
                "inner join film on idfilm = film.id where film.id in\n" +
                "(select id from film where title = '" + filmTitle + "'))");
    }

    public ArrayList<Actor> getActorsWhoWereDirectors() {
        return helper.getResultForSelectingActors("select a.id, a.name, a.birthdate from actor a inner join director d on a.name = d.name");
    }

    public ArrayList<Actor> getActorsPlayedInMoreThanNFilm(int filmsNumber) {
        return helper.getResultForSelectingActors("select a.id, a.name, a.birthdate from actor a \n" +
                "inner join film_actors fa on a.id = fa.idactor \n" +
                "group by fa.idactor having count(idfilm) >= " + filmsNumber + ";");
    }

    public ArrayList<Film> getFilmsReleasedInGivenPeriod(int yearFrom, int yearTo) {
        ArrayList<Film> listFilms = new ArrayList<>();
        Statement st = null;
        try {
            st = helper.getStatement();
            ResultSet rs;
            try {
                rs = st.executeQuery("select * from film where YEAR(release_year) between " + yearFrom + " and " + yearTo + ";");
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String title = rs.getString(2);
                    Date releaseYear = rs.getDate(3);
                    String releaseCountry = rs.getString(4);
                    ArrayList<Actor> actors = helper.getArrayListOfPersonsFromFilm(id, "{call select_actors_from_film(?)}");
                    ArrayList<Director> director = helper.getArrayListOfPersonsFromFilm(id, "{call select_directors_from_film(?)}");
                    listFilms.add(new Film(id, title, releaseYear, releaseCountry, actors, director));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            helper.closeStatement(st);
        }
        return listFilms.size() > 0 ? listFilms : null;
    }

    public int deletingFilmsReleasedOverNYearsAgo(int yearsAgo) {
        int deletedRows = 0;
        Statement st = null;
        try {
            st = helper.getStatement();
            try {
                deletedRows = st.executeUpdate("delete from film where release_year <= subdate(curdate(), interval " + yearsAgo + " YEAR)");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            helper.closeStatement(st);
        }
        return deletedRows;
    }

    public User findUser(String login, String password) {
        User user = null;
        Statement st = null;
        try {
            st = helper.getStatement();
            ResultSet rs;
            try {
                rs = st.executeQuery("select * from user where login = '" + login + "' and password = '" + password + "';");
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    String pass = rs.getString(3);
                    String role = rs.getString(4);
                    user = new User(id, name, pass, role);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            helper.closeStatement(st);
        }
        return user;
    }
}
