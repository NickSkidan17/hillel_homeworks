package hw5;

public class Triangle {
    double firstSide, secondSide, thirdSide;
    double perimeter, square;
    double maxPerimeter = 0, minPerimeter = 100, maxSquare = 0, minSquare = 100;

    Triangle(CoordinatePoint cp1, CoordinatePoint cp2, CoordinatePoint cp3) {
        this.firstSide = cp1.lengthBetweenPoints(cp2);
        this.secondSide = cp1.lengthBetweenPoints(cp3);
        this.thirdSide = cp2.lengthBetweenPoints(cp3);
    }

    Triangle() {

    }

    double getPerimeter(Triangle triangle) {
        perimeter = triangle.firstSide + triangle.secondSide + triangle.thirdSide;
        return perimeter;
    }

    double getSquare(Triangle triangle) {
        double halfPerimeter = (triangle.firstSide + triangle.secondSide + triangle.thirdSide) / 2;
        square = Math.sqrt(halfPerimeter * (halfPerimeter - triangle.firstSide) * (halfPerimeter - triangle.secondSide) * (halfPerimeter - triangle.thirdSide));
        return square;
    }

    double getMinPerimeter(Triangle[] triangles) {
        for (Triangle triangle : triangles) {
            if (triangle != null) {
                double perimeter = triangle.firstSide + triangle.secondSide + triangle.thirdSide;
                if (perimeter < minPerimeter) {
                    minPerimeter = perimeter;
                }
            }
        }
        return minPerimeter;
    }

    double getMaxPerimeter(Triangle[] triangles) {
        for (Triangle triangle : triangles) {
            if (triangle != null) {
                double perimeter = triangle.firstSide + triangle.secondSide + triangle.thirdSide;
                if (perimeter > maxPerimeter) {
                    maxPerimeter = perimeter;
                }
            }
        }
        return maxPerimeter;
    }

    double getMinSquare(Triangle[] triangles) {
        for (Triangle triangle : triangles) {
            if (triangle != null) {
                double halfPerimeter = (triangle.firstSide + triangle.secondSide + triangle.thirdSide) / 2;
                double square = Math.sqrt(halfPerimeter * (halfPerimeter - triangle.firstSide) * (halfPerimeter - triangle.secondSide) * (halfPerimeter - triangle.thirdSide));
                if (square < minSquare) {
                    minSquare = square;
                }
            }
        }
        return minSquare;
    }

    double getMaxSquare(Triangle[] triangles) {
        for (Triangle triangle : triangles) {
            if (triangle != null) {
                double halfPerimeter = (triangle.firstSide + triangle.secondSide + triangle.thirdSide) / 2;
                double square = Math.sqrt(halfPerimeter * (halfPerimeter - triangle.firstSide) * (halfPerimeter - triangle.secondSide) * (halfPerimeter - triangle.thirdSide));
                if (square > maxSquare) {
                    maxSquare = square;
                }
            }
        }
        return maxSquare;
    }

    boolean isTriangleEquilateral() {
        if (firstSide == secondSide && firstSide == thirdSide && secondSide == thirdSide) {
            return true;
        } else {
            return false;
        }
    }

    boolean isTriangleRectangular() {
        if ((Math.pow(firstSide, 2) == Math.pow(secondSide, 2) + Math.pow(thirdSide, 2)) || (Math.pow(secondSide, 2) == Math.pow(firstSide, 2) + Math.pow(thirdSide, 2)) || (Math.pow(thirdSide, 2) == Math.pow(firstSide, 2) + Math.pow(secondSide, 2))) {
            return true;
        } else {
            return false;
        }
    }

    boolean isTriangleIsosceles() {
        if (firstSide == secondSide || secondSide == thirdSide || firstSide == thirdSide && !isTriangleEquilateral()) {
            return true;
        } else {
            return false;
        }
    }


    boolean isTriangleArbitrary() {
        if (!isTriangleEquilateral() && !isTriangleIsosceles() && !isTriangleRectangular()) {
            return true;
        } else {
            return false;
        }
    }

    void printTriangle(Triangle triangle) {
        System.out.printf("%s%.2f%s%.2f%s%.2f%s%.2f%s%.2f\n", "Triangle with firstside: ", triangle.firstSide, "; secondside: ", triangle.secondSide, "; thirdside: ", triangle.thirdSide, "; perimeter: ", getPerimeter(triangle), "; square: ", getSquare(triangle));
    }

    void printParameterAndSquare(Triangle[] triangle) {
        System.out.printf("%s%.2f\n", "Minimal perimeter from current group of triangles: ", getMinPerimeter(triangle));
        System.out.printf("%s%.2f\n", "Maximum perimeter from current group of triangles: ", getMaxPerimeter(triangle));
        System.out.printf("%s%.2f\n", "Minimal square from current group of triangles: ", getMinSquare(triangle));
        System.out.printf("%s%.2f\n", "Maximum square from current group of triangles: ", getMaxSquare(triangle));
    }
}
