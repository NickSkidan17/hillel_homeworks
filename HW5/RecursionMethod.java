package hw5;

public class RecursionMethod {
    static String recursionN(int n) {
        if (n == 1)
            return "1";
        return recursionN(n - 1) + " " + n;
    }

    static int recursionSum(int n) {
        if (n < 10)
            return n;
        else
            return n % 10 + recursionSum(n / 10);
    }
}
