/*
Определить класс Треугольник на плоскости. Вычислить площадь и периметр
треугольника. Создать массив объектов и подсчитать
количество треугольников разного типа (равносторонний, равнобедренный,
прямоугольный, произвольный). Определить для каждой группы наибольший
и наименьший по площади (периметру) объект
 */

package hw5;

public class TestTriangle {
    public static void main(String[] args) {
        int counterEquilateralTriangles = 0, counterIsoscelesTriangles = 0, counterRectangularTriangles = 0, counterArbitraryTriangles = 0;
        Triangle tr = new Triangle();
        CoordinatePoint[] arrayCoordinatePoints = {new CoordinatePoint(-0.5, 0.0),
                new CoordinatePoint(1.0, 0.0),
                new CoordinatePoint(0.0, 3.0),
                new CoordinatePoint(6.0, 0.0),
                new CoordinatePoint(7.0, 0.0),
                new CoordinatePoint(6.1, -2.0),
                new CoordinatePoint(0.0, 0.0),
                new CoordinatePoint(0.0, 4.0),
                new CoordinatePoint(3.0, 0.0),
                new CoordinatePoint(-1.0, -3.0),
                new CoordinatePoint(-1.0, 0.0),
                new CoordinatePoint(4.0, -3.0),
                new CoordinatePoint(2.0, 0.0),
                new CoordinatePoint(5.0, 3.0),
                new CoordinatePoint(5.0, -3.0),
                new CoordinatePoint(8.0, 9.0),
                new CoordinatePoint(10.0, 2.0),
                new CoordinatePoint(6.0, 2.0),
                new CoordinatePoint(-6.0, 0.0),
                new CoordinatePoint(-3.0, -1.0),
                new CoordinatePoint(-5.0, 6.0),
                new CoordinatePoint(-1.5, -1.5),
                new CoordinatePoint(1.0, -1.0),
                new CoordinatePoint(2.5, 7.0),
                new CoordinatePoint(5.4, 3.0),
                new CoordinatePoint(-3.0, 7.8),
                new CoordinatePoint(-5.2, -4.9),
                new CoordinatePoint(-2.0, 2.0),
                new CoordinatePoint(-3.0, 6.0),
                new CoordinatePoint(-1.0, 6.0)};

        Triangle[] arrayTriangle = new Triangle[10];
        arrayTriangle[0] = new Triangle(arrayCoordinatePoints[0], arrayCoordinatePoints[1], arrayCoordinatePoints[2]);
        arrayTriangle[1] = new Triangle(arrayCoordinatePoints[3], arrayCoordinatePoints[4], arrayCoordinatePoints[5]);
        arrayTriangle[2] = new Triangle(arrayCoordinatePoints[6], arrayCoordinatePoints[7], arrayCoordinatePoints[8]);
        arrayTriangle[3] = new Triangle(arrayCoordinatePoints[9], arrayCoordinatePoints[10], arrayCoordinatePoints[11]);
        arrayTriangle[4] = new Triangle(arrayCoordinatePoints[12], arrayCoordinatePoints[13], arrayCoordinatePoints[14]);
        arrayTriangle[5] = new Triangle(arrayCoordinatePoints[15], arrayCoordinatePoints[16], arrayCoordinatePoints[17]);
        arrayTriangle[6] = new Triangle(arrayCoordinatePoints[18], arrayCoordinatePoints[19], arrayCoordinatePoints[20]);
        arrayTriangle[7] = new Triangle(arrayCoordinatePoints[21], arrayCoordinatePoints[22], arrayCoordinatePoints[23]);
        arrayTriangle[8] = new Triangle(arrayCoordinatePoints[24], arrayCoordinatePoints[25], arrayCoordinatePoints[26]);
        arrayTriangle[9] = new Triangle(arrayCoordinatePoints[27], arrayCoordinatePoints[28], arrayCoordinatePoints[29]);

        Triangle[] arrayEquilateralTriangles = new Triangle[arrayTriangle.length];
        Triangle[] arrayIsoscelesTriangles = new Triangle[arrayTriangle.length];
        Triangle[] arrayRectangularTriangles = new Triangle[arrayTriangle.length];
        Triangle[] arrayArbitraryTriangles = new Triangle[arrayTriangle.length];

        System.out.println("Equilateral triangles:");
        for (int i = 0; i < arrayTriangle.length; i++) {
            for (int j = i; j < arrayEquilateralTriangles.length; j++) {
                if (arrayTriangle[i].isTriangleEquilateral()) {
                    counterEquilateralTriangles++;
                    arrayEquilateralTriangles[j] = arrayTriangle[i];
                    break;
                }
            }
        }
        System.out.println("Reader of equilateral triangles: " + counterEquilateralTriangles);
        for (Triangle triangle : arrayEquilateralTriangles) {
            if (triangle != null) {
                tr.printTriangle(triangle);
            }
        }
        if (counterEquilateralTriangles != 0) {
            tr.printParameterAndSquare(arrayEquilateralTriangles);
        }
        System.out.println();

        System.out.println("Rectangular triangles:");
        for (int i = 0; i < arrayTriangle.length; i++) {
            for (int j = i; j < arrayRectangularTriangles.length; j++) {
                if (arrayTriangle[i].isTriangleRectangular()) {
                    counterRectangularTriangles++;
                    arrayRectangularTriangles[j] = arrayTriangle[i];
                    break;
                }
            }
        }
        System.out.println("Reader of rectangular triangles: " + counterRectangularTriangles);
        for (Triangle triangle : arrayRectangularTriangles) {
            if (triangle != null) {
                tr.printTriangle(triangle);
            }
        }
        if (counterRectangularTriangles != 0) {
            tr.printParameterAndSquare(arrayRectangularTriangles);
        }
        System.out.println();

        System.out.println("Isosceles triangles:");
        for (int i = 0; i < arrayTriangle.length; i++) {
            for (int j = i; j < arrayIsoscelesTriangles.length; j++) {
                if (arrayTriangle[i].isTriangleIsosceles()) {
                    counterIsoscelesTriangles++;
                    arrayIsoscelesTriangles[j] = arrayTriangle[i];
                    break;
                }
            }
        }
        System.out.println("Reader of isosceles triangles: " + counterIsoscelesTriangles);
        for (Triangle triangle : arrayIsoscelesTriangles) {
            if (triangle != null) {
                tr.printTriangle(triangle);
            }
        }
        if (counterIsoscelesTriangles != 0) {
            tr.printParameterAndSquare(arrayIsoscelesTriangles);
        }
        System.out.println();

        System.out.println("Arbitrary triangles:");
        for (int i = 0; i < arrayTriangle.length; i++) {
            for (int j = i; j < arrayArbitraryTriangles.length; j++) {
                if (arrayTriangle[i].isTriangleArbitrary()) {
                    counterArbitraryTriangles++;
                    arrayArbitraryTriangles[j] = arrayTriangle[i];
                    break;
                }
            }
        }
        System.out.println("Reader of arbitrary triangles: " + counterArbitraryTriangles);
        for (Triangle triangle : arrayArbitraryTriangles) {
            if (triangle != null) {
                tr.printTriangle(triangle);
            }
        }
        if (counterArbitraryTriangles != 0) {
            tr.printParameterAndSquare(arrayArbitraryTriangles);
        }
        System.out.println();
    }
}
