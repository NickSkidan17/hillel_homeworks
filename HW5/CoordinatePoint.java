package hw5;

public class CoordinatePoint {
    private double x;
    private double y;

    CoordinatePoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    double lengthBetweenPoints(CoordinatePoint cp) {
        return Math.sqrt((Math.pow(cp.x - x, 2)) + (Math.pow(cp.y - y, 2)));
    }
}
