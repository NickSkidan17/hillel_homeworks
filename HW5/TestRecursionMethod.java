/*
1. Дано натуральное число n (обычная переменная int). Рекурсивно вывести все числа от 1 до n. (Ввод 5 -> вывод 1 2 3 4 5, делать отдельным классом и в нем метод
2. Рекурсивно вычислить сумму цифр числа (делать отдельным классом и в нем метод)
 */

package hw5;

public class TestRecursionMethod {
    public static void main(String[] args) {
        System.out.print(RecursionMethod.recursionN(17) + "\n" + RecursionMethod.recursionSum(267553));
    }
}
