package hw7.taxipool;

import hw7.cars.Car;

public class TaxiPoolReal implements TaxiPool {
    private Car[] cars;

    public TaxiPoolReal(Car[] cars) {
        this.cars = cars;
    }

    @Override
    public int getTaxiPoolPrice() {
        int taxiPoolPrice = 0;
        for (Car car : cars) {
            taxiPoolPrice += car.getPrice();
        }
        return taxiPoolPrice;
    }

    @Override
    public Car[] sortTaxiPoolByFuelConsumption() {
        for (int i = 0; i < cars.length; i++) {
            for (int j = i + 1; j < cars.length; j++) {
                if (cars[j].getFuelConsumption() < cars[i].getFuelConsumption()) {
                    Car var = cars[i];
                    cars[i] = cars[j];
                    cars[j] = var;
                }
            }
        }
        return cars;
    }

    @Override
    public Car[] selectCarsInSpeedInterval(int minSpeed, int maxSpeed) {
        int counter = 0;
        Car[] carsInSpeedInterval = new Car[0];
        for (Car car : cars) {
            if (car.getTopSpeed() >= minSpeed && car.getTopSpeed() <= maxSpeed) {
                Car[] someArray = new Car[carsInSpeedInterval.length + 1];
                for (int i = 0; i < carsInSpeedInterval.length; i++) {
                    someArray[i] = carsInSpeedInterval[i];
                }
                carsInSpeedInterval = someArray;
                someArray[counter] = car;
                counter++;
            }
        }
        return carsInSpeedInterval;
    }

    public Car[] getCars() {
        return cars;
    }

    public void setCars(Car[] cars) {
        this.cars = cars;
    }
}