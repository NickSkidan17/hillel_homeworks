package hw7.taxipool;

import hw7.cars.Car;

public interface TaxiPool {

    int getTaxiPoolPrice();

    Car[] sortTaxiPoolByFuelConsumption();

    Car[] selectCarsInSpeedInterval(int minSpeed, int maxSpeed);

}
