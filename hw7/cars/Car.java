package hw7.cars;

public interface Car {

    String getMark();

    void setMark(String mark);

    String getModel();

    void setModel(String model);

    int getTopSpeed();

    void setTopSpeed(int topSpeed);

    double getFuelConsumption();

    void setFuelConsumption(int fuelConsumption);

    int getPrice();

    void setPrice(int price);

}
