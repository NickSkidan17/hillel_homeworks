package hw7.cars;

public class CargoVehicle implements Car {
    private String mark;
    private String model;
    private int topSpeed;
    private double fuelConsumption;
    private int price;
    private double tonnage;
    private String wheelScheme;

    public CargoVehicle(String mark, String model, int topSpeed, int fuelConsumption, int price, double tonnage, String wheelScheme) {
        this.mark = mark;
        this.model = model;
        this.topSpeed = topSpeed;
        this.fuelConsumption = fuelConsumption;
        this.price = price;
        this.tonnage = tonnage;
        this.wheelScheme = wheelScheme;
    }

    @Override
    public String toString() {
        return "mark: " + mark + "; model: " + model + "; topSpeed: " + topSpeed + " km/h; fuelConsumption: " + fuelConsumption + " liters/100km; price: " + price + "; tonnage: " + tonnage + " kg; wheelScheme: " + wheelScheme;
    }

    @Override
    public String getMark() {
        return mark;
    }

    @Override
    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public int getTopSpeed() {
        return topSpeed;
    }

    @Override
    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    @Override
    public double getFuelConsumption() {
        return fuelConsumption;
    }

    @Override
    public void setFuelConsumption(int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public void setPrice(int price) {
        this.price = price;
    }

    public double getTonnage() {
        return tonnage;
    }

    public void setTonnage(double tonnage) {
        this.tonnage = tonnage;
    }

    public String getWheelScheme() {
        return wheelScheme;
    }

    public void setWheelScheme(String wheelScheme) {
        this.wheelScheme = wheelScheme;
    }
}
