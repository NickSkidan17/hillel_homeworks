package hw7.cars;

public class PassengerVehicle implements Car {
    private String mark;
    private String model;
    private int topSpeed;
    private double fuelConsumption;
    private int price;
    private String fuelType;
    private String transmissionType;
    private int passengerSeatsNumber;

    public PassengerVehicle(String mark, String model, int topSpeed, int fuelConsumption, int price, String fuelType, String transmissionType, int passengerSeatsNumber) {
        this.mark = mark;
        this.model = model;
        this.topSpeed = topSpeed;
        this.fuelConsumption = fuelConsumption;
        this.price = price;
        this.fuelType = fuelType;
        this.transmissionType = transmissionType;
        this.passengerSeatsNumber = passengerSeatsNumber;
    }

    @Override
    public String toString() {
        return "mark: " + mark + "; model: " + model + "; topSpeed: " + topSpeed + " km/h; fuelConsumption: " + fuelConsumption + " liters/100km; price: " + price + "; fuelType: " + fuelType + "; transmissionType: " + transmissionType;
    }


    @Override
    public String getMark() {
        return mark;
    }

    @Override
    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public int getTopSpeed() {
        return topSpeed;
    }

    @Override
    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    @Override
    public double getFuelConsumption() {
        return fuelConsumption;
    }

    @Override
    public void setFuelConsumption(int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public void setPrice(int price) {
        this.price = price;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(String transmissionType) {
        this.transmissionType = transmissionType;
    }

    public int getPassengerSeatsNumber() {
        return passengerSeatsNumber;
    }

    public void setPassengerSeatsNumber(int passengerSeatsNumber) {
        this.passengerSeatsNumber = passengerSeatsNumber;
    }
}