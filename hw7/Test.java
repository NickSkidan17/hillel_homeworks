/*
Переделать предыдущее задание.

Сделать Car интерфейсом (например методы int getPrice(); и void setPrice(int price);).
Создать несколько конкретных автомобилей, которые будут реализовывать интерфейс.
Сделать таксопарк интерфейсом, определить в нем действия, которые должен выполнять таксопарк.
Создать конкретный такосопарк который будет реализовывать интерфейс таксопарка.
Грамотно распределить по пакетам классы.
 */

package hw7;

import hw7.cars.*;
import hw7.taxipool.*;

public class Test {
    public static void main(String[] args) {
        Car[] cars = new Car[6];
        cars[0] = new CargoVehicle("MAN", "TGS", 160, 25, 95000, 90000, "6*6");
        cars[1] = new CargoVehicle("Foton Auman", "H5", 195, 30, 80000, 50000, "4*2");
        cars[2] = new OffroadVehicle("BMW", "X5", 240, 20, 75000, "AWD", 1000);
        cars[3] = new OffroadVehicle("Ford", "Expedition", 220, 27, 70000, "AWD", 500);
        cars[4] = new PassengerVehicle("Peugeot", "Rifter", 200, 17, 40000, "petrol", "manual", 7);
        cars[5] = new PassengerVehicle("Bentley", "Continental", 300, 24, 200000, "petrol", "semi-automatic", 4);

        TaxiPoolReal taxipool = new TaxiPoolReal(cars);

        System.out.println("Total cost of taxi pool is: " + taxipool.getTaxiPoolPrice() + "$.\n");

        System.out.println("Taxi pool after sorting by fuel consumption: ");
        for (Car car : taxipool.sortTaxiPoolByFuelConsumption()) {
            System.out.println(car);
        }
        System.out.println();

        System.out.println("Cars in a given speed interval: ");
        for (Car car : taxipool.selectCarsInSpeedInterval(160, 210)) {
            System.out.println(car);
        }
    }
}
