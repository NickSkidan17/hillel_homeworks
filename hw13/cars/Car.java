package hw13.cars;

public interface Car {

    String getMark();

    void setMark(String mark);

    String getModel();

    void setModel(String model);

    int getTopSpeed();

    void setTopSpeed(int topSpeed);

    Integer getFuelConsumption();

    void setFuelConsumption(Integer fuelConsumption);

    int getPrice();

    void setPrice(int price);

}

