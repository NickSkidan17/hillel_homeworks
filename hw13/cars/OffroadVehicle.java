package hw13.cars;

public class OffroadVehicle implements Car {
    private String mark;
    private String model;
    private int topSpeed;
    private Integer fuelConsumption;
    private int price;
    private String driveType;
    private double trunkVolume;

    public OffroadVehicle(String mark, String model, int topSpeed, Integer fuelConsumption, int price, String driveType, double trunkVolume) {
        this.mark = mark;
        this.model = model;
        this.topSpeed = topSpeed;
        this.fuelConsumption = fuelConsumption;
        this.price = price;
        this.driveType = driveType;
        this.trunkVolume = trunkVolume;
    }

    @Override
    public String toString() {
        return "mark: " + mark + "; model: " + model + "; topSpeed: " + topSpeed + " km/h; fuelConsumption: " + fuelConsumption + " liters/100km; price: " + price + "; driveType: " + driveType + "; trunkVolume: " + trunkVolume + " liters.";
    }

    @Override
    public String getMark() {
        return mark;
    }

    @Override
    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public int getTopSpeed() {
        return topSpeed;
    }

    @Override
    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    @Override
    public Integer getFuelConsumption() {
        return fuelConsumption;
    }

    @Override
    public void setFuelConsumption(Integer fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    @Override
    public int getPrice() {
        return price;
    }

    @Override
    public void setPrice(int price) {
        this.price = price;
    }

    public String getDriveType() {
        return driveType;
    }

    public void setDriveType(String driveType) {
        this.driveType = driveType;
    }

    public double getTrunkVolume() {
        return trunkVolume;
    }

    public void setTrunkVolume(double trunkVolume) {
        this.trunkVolume = trunkVolume;
    }

}
