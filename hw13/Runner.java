/*
Переделать Таксопарк. Реализовать метод по сортировке автомобилей по
расходу топлива с помощью Arrays.sort, который принимает компаратор.
Компаратор реализовать тремя способами: реализация интерфейса,
лямбда-выражение и ссылка на метод
 */

package hw13;

import hw13.cars.*;
import hw13.taxipool.TaxiPoolImpl;

import java.util.ArrayList;

public class Runner {
    public static void main(String[] args) {
        ArrayList<Car> cars = new ArrayList<>();
        cars.add(new CargoVehicle("MAN", "TGS", 160, 25, 95000, 90000, "6*6"));
        cars.add(new CargoVehicle("Foton Auman", "H5", 195, 30, 80000, 50000, "4*2"));
        cars.add(new OffroadVehicle("BMW", "X5", 240, 20, 75000, "AWD", 1000));
        cars.add(new OffroadVehicle("Ford", "Expedition", 220, 27, 70000, "AWD", 500));
        cars.add(new PassengerVehicle("Peugeot", "Rifter", 200, 17, 40000, "petrol", "manual", 7));
        cars.add(new PassengerVehicle("Bentley", "Continental", 300, 24, 200000, "petrol", "semi-automatic", 4));

        TaxiPoolImpl taxipool = new TaxiPoolImpl(cars);

        System.out.println("Total cost of taxi pool is: " + taxipool.getTaxiPoolPrice() + "$.\n");

        System.out.println("Taxi pool after sorting by fuel consumption: ");
        for (Car car : taxipool.sortTaxiPoolByFuelConsumption()) {
            System.out.println(car);
        }
        System.out.println();

        System.out.println("Cars in a given speed interval: ");
        for (Car car : taxipool.selectCarsInSpeedInterval(160, 210)) {
            System.out.println(car);
        }
    }
}
