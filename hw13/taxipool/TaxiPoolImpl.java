package hw13.taxipool;

import hw13.cars.Car;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class TaxiPoolImpl implements TaxiPool {
    private ArrayList<Car> cars;
    // implementing functional interface Comparator using anonymous class:
    private Comparator<Car> compImpl = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {
            return car1.getFuelConsumption().compareTo(car2.getFuelConsumption());
        }
    };
    // using lambda:
    private Comparator<Car> compLambda = (Car car1, Car car2) -> car1.getFuelConsumption().compareTo(car2.getFuelConsumption());

    public TaxiPoolImpl(ArrayList<Car> cars) {
        this.cars = cars;
    }

    @Override
    public ArrayList<Car> sortTaxiPoolByFuelConsumption() {
        // using reference to a method
        Collections.sort(cars, SortByFuelConsumption::compareByFuelConsumption);
        return cars;
    }

    @Override
    public int getTaxiPoolPrice() {
        int taxiPoolPrice = 0;
        for (Car car : cars) {
            taxiPoolPrice += car.getPrice();
        }
        return taxiPoolPrice;
    }

    @Override
    public ArrayList<Car> selectCarsInSpeedInterval(int minSpeed, int maxSpeed) {
        ArrayList<Car> carsInSpeedInterval = new ArrayList<>();
        for (Car car : cars) {
            if (car.getTopSpeed() >= minSpeed && car.getTopSpeed() <= maxSpeed) {
                carsInSpeedInterval.add(car);
            }
        }
        return carsInSpeedInterval;
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    public void setCars(ArrayList<Car> cars) {
        this.cars = cars;
    }
}
