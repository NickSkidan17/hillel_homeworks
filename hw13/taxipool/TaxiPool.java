package hw13.taxipool;

import hw13.cars.Car;

import java.util.ArrayList;

public interface TaxiPool {

    int getTaxiPoolPrice();

    ArrayList<Car> sortTaxiPoolByFuelConsumption();

    ArrayList<Car> selectCarsInSpeedInterval(int minSpeed, int maxSpeed);

}
