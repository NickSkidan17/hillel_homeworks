package hw13.taxipool;

import hw13.cars.Car;

public class SortByFuelConsumption {
    static int compareByFuelConsumption(Car car1, Car car2) {
        return car1.getFuelConsumption().compareTo(car2.getFuelConsumption());
    }
}
