package hw12;

public class Node<T extends Comparable<T>> {
    Node next;
    T data;

    public Node(Node next, T data) {
        this.next = next;
        this.data = data;
    }
}
