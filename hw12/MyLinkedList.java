package hw12;

import java.util.Iterator;

public class MyLinkedList<T extends Comparable<T>> implements Iterable<T> {
    private Node head;
    private int size;

    public MyLinkedList(T[] data) {
        for (T t : data) {
            addLastElement(t);
        }
    }

    public void addFirstElement(T data) {
        head = new Node(head, data);
        size++;
    }

    public void addLastElement(T data) {
        if (isEmpty()) {
            addFirstElement(data);
        } else {
            Node last = getNodeByIndex(size - 1);
            last.next = new Node(null, data);
            size++;
        }
    }

    public void addNElement(int index, T data) {
        checkElementIndexForAdd(index);
        if (isFirstElementOperation(index)) {
            addFirstElement(data);
        } else if (isLastElementOperation(index)) {
            addLastElement(data);
        } else {
            Node temp = getNodeByIndex(index);
            temp.next = new Node(temp.next, data);
            size++;
        }
    }

    public void deleteFirstElement() {
        if (!isEmpty()) {
            head = head.next;
            size--;
        }
    }

    public void deleteLastElement() {
        if (size == 1) {
            deleteFirstElement();
        } else if (!isEmpty()) {
            Node previous = getNodeByIndex(size - 2);
            previous.next = null;
            size--;
        }
    }

    public void deleteNElement(int index) {
        checkElementIndexForDelete(index);
        if (isFirstElementOperation(index)) {
            deleteFirstElement();
        } else if (isLastElementOperation(index)) {
            deleteLastElement();
        } else {
            Node temp = getNodeByIndex(index - 1);
            temp.next = temp.next.next;
            size--;
        }
    }

    public void replaceElements(int firstElementIndex, int secondElementIndex) {
        checkElementIndexesForReplace(firstElementIndex, secondElementIndex);
        Node firstElementIndexNode = getNodeByIndex(firstElementIndex);
        Node secondElementIndexNode = getNodeByIndex(secondElementIndex);
        if (firstElementIndex == secondElementIndex) {
            return;
        }
        Node firstElementIndexNodeBefore = getNodeByIndex(firstElementIndex - 1);
        Node secondElementIndexNodeBefore = getNodeByIndex(secondElementIndex - 1);
        if (isFirstElementOperation(firstElementIndex)) {
            head = secondElementIndexNode;
        } else {
            firstElementIndexNodeBefore.next = secondElementIndexNode;
        }
        if (isFirstElementOperation(secondElementIndex)) {
            head = firstElementIndexNode;
        } else {
            secondElementIndexNodeBefore.next = firstElementIndexNode;
        }
        Node temp = secondElementIndexNode.next;
        secondElementIndexNode.next = firstElementIndexNode.next;
        firstElementIndexNode.next = temp;
    }

    public int getSize() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void printList() {
        if (isEmpty()) {
            System.out.println("List is empty");
        } else {
            Node node = head;
            while (node != null) {
                System.out.print(node.data + " ");
                node = node.next;
            }
            System.out.println();
        }
    }

    public void quickSort() {
        if (isEmpty()) {
            System.out.println("List is empty");
        }
        int start = 0;
        int end = size - 1;
        doSort(start, end);
    }

    private void doSort(int fromIndex, int toIndex) {
        if (fromIndex >= toIndex) {
            return;
        }
        int i = fromIndex;
        int j = toIndex;
        int pivot = (i + j) / 2;
        Node temp = getNodeByIndex(pivot);
        while (i < j) {
            while (i < pivot && getNodeByIndex(i).data.compareTo(temp.data) <= 0) {
                i++;
            }
            while (j > pivot && getNodeByIndex(j).data.compareTo(temp.data) >= 0) {
                j--;
            }
            if (i < j) {
                replaceElements(i, j);
                if (i == pivot) {
                    pivot = j;
                } else if (j == pivot) {
                    pivot = i;
                }
            }
        }
        doSort(fromIndex, pivot);
        doSort(pivot + 1, toIndex);
    }

    private Node getNodeByIndex(int index) {
        Node temp = head;
        for (int i = 0; i < index; i++) {
            temp = temp.next;
        }
        return temp;
    }

    private boolean isFirstElementOperation(int index) {
        return index == 0;
    }

    private boolean isLastElementOperation(int index) {
        return index == size;
    }

    private void checkElementIndexForAdd(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }
    }

    private void checkElementIndexForDelete(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
    }

    private void checkElementIndexesForReplace(int firstElementIndex, int secondElementIndex) {
        checkElementIndexForDelete(firstElementIndex);
        checkElementIndexForDelete(secondElementIndex);
    }

    @Override
    public Iterator<T> iterator() {
        return new MyLinkedListIterator();
    }

    private class MyLinkedListIterator implements Iterator<T> {
        private Node current = head;

        public T next() throws IndexOutOfBoundsException {
            T result = (T) current.data;
            current = current.next;
            return result;
        }

        public boolean hasNext() {
            return current != null;
        }
    }
}
