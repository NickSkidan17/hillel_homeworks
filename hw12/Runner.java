/*
Реализовать структуру данных односвязного списка.
Список сделать обобщенным (Generic).

Функции списка:

Инициализация списка(пустого и на основании массива)
Добавление узла в список в начало, конец и внутрь по нидексу
Удаление узла из начала, конца и n-го элемента
Вывод элементов списка (toString())
Взаимообмен двух узлов списка (по двум идексам)
Количество элементов в списке
Проверить список на пустоту

Индекс в ноде не хранить!
Реализовать в нашей версии LinkedList'а итератор.
 */

package hw12;

import java.util.Iterator;

public class Runner {
    public static void main(String[] args) {
        MyLinkedList<String> linkedList = new MyLinkedList<>(new String[]{"ab", "cv", "rh", "sr", "kl", "xf", "ny"});
        System.out.println("Initial list: ");
        linkedList.printList();
        linkedList.addFirstElement("tu");
        linkedList.addLastElement("ml");
        linkedList.addNElement(2, "qw");
        System.out.println("List after adding three elements: ");
        linkedList.printList();
        linkedList.deleteNElement(1);
        linkedList.deleteFirstElement();
        linkedList.deleteLastElement();
        System.out.println("List after deleting three elements: ");
        linkedList.printList();
        linkedList.replaceElements(0, 2);
        System.out.println("List after replacing two elements: ");
        linkedList.printList();
        linkedList.quickSort();
        System.out.println("Sorted list: ");
        linkedList.printList();
        System.out.println("List size: " + linkedList.getSize());
        Iterator<String> it = linkedList.iterator();
        while(it.hasNext()){
            String string = it.next();
            System.out.print(string + " ");
        }
    }
}
