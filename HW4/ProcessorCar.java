package hw4;

public class ProcessorCar {
    private int currentYear = 2018;
    private Car[] garage;

    ProcessorCar(Car[] cars) {
        this.garage = cars;
    }

    ProcessorCar() {
        garage = new Car[0];
    }

    void findCarByMark(Car[] cars, String mark) {
        System.out.printf("%s%s%s\n", "List of ", mark, " cars (by id):");
        for (int i = 0; i < cars.length; i++) {
            if (mark.equals(cars[i].getMark())) {
                print(cars[i]);
            }
        }
    }

    void findCarByModelAndExploitationPeriod(Car[] cars, String model, int exploitationPeriod) {
        System.out.printf("%s%s%s%d%s\n", "List of ", model, " cars, which are operated for more than ", exploitationPeriod, " years (by id):");
        for (int i = 0; i < cars.length; i++) {
            if (model.equals(cars[i].getModel()) && exploitationPeriod < (currentYear - cars[i].getReleaseYear())) {
                print(cars[i]);
            }
        }
    }

    void findCarByReleaseYearAndPrice(Car[] cars, int releaseYear, int price) {
        System.out.printf("%s%d%s%d%s\n", "List of cars produced in ", releaseYear, ", the price of which is more than ", price, " (by id):");
        for (int i = 0; i < cars.length; i++) {
            if (releaseYear == cars[i].getReleaseYear() && price < cars[i].getPrice()) {
                print(cars[i]);
            }
        }
    }

    void findCarByMark(String mark) {
        if (garage.length <= 0) {
            return;
        }
        findCarByMark(garage, mark);
    }

    void findCarByModelAndExploitationPeriod(String model, int exploitationPeriod) {
        if (garage.length <= 0) {
            return;
        }
        findCarByModelAndExploitationPeriod(garage, model, exploitationPeriod);
    }

    void findCarByReleaseYearAndPrice(int releaseYear, int price) {
        if (garage.length <= 0) {
            return;
        }
        findCarByReleaseYearAndPrice(garage, releaseYear, price);
    }

    void print(Car car) {
        System.out.println("Car [id: " + car.getId() + ", mark: " + car.getMark() + ", model: " + car.getModel() + ", releaseYear: " + car.getReleaseYear() + ", color: " + car.getColor() + ", price: " + car.getPrice() + ", registrationNumber: " + car.getRegistrationNumber() + "];");
    }
}
