package hw16;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WorkWithTextByPattern {

    private static final String FILE_IN = "C:\\JavaStudy\\JEStudy\\src\\hw16\\txtfiles\\test.txt";

    public Map<String, Integer> wordAndItsNumber() {
        HashMap<String, Integer> map = new HashMap<>();
        Pattern pattern = Pattern.compile("[a-z]+");
        Matcher matcher = pattern.matcher(textToString().toLowerCase());
        while (matcher.find()) {
            if (map.containsKey(matcher.group())) {
                map.put(matcher.group(), map.get(matcher.group()) + 1);
                continue;
            }
            map.put(matcher.group(), 1);
        }
        return map;
    }

    public int punctuationMarksNumber() {
        Pattern pattern = Pattern.compile("[),.;(:!?-]");
        Matcher matcher = pattern.matcher(textToString());
        int counter = 0;
        while (matcher.find()) {
            counter++;
        }
        return counter;
    }

    public String replaceSpecificLetterInWord() {
        StringBuilder sb = new StringBuilder();
        Pattern pattern = Pattern.compile("[A-Za-z.,!)(:?;-]+");
        Matcher matcher = pattern.matcher(textToString());
        int index = 2;
        char symbolReplace = 'A';
        while (matcher.find()) {
            String str = matcher.group();
            if (str.length() > index) {
                sb.append(str, 0, index).append(symbolReplace).append(str, index + 1, str.length());
            } else {
                sb.append(str);
            }
            sb.append(" ");
        }
        return sb.toString();
    }

    public HashSet<String> wordsWithEqualsFirstAndLastLetters() {
        HashSet<String> result = new HashSet<>();
        Pattern pattern = Pattern.compile("[A-Za-z]+");
        Matcher matcher = pattern.matcher(textToString());
        while (matcher.find()) {
            if (isFirstAndLastLettersEquals(matcher.group())) {
                result.add(matcher.group());
            }
        }
        return result;
    }

    private String textToString() {
        File file = new File(FILE_IN);
        String result = "";
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                result += line;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private boolean isFirstAndLastLettersEquals(String string) {
        return string.length() > 1 && string.charAt(0) == string.charAt(string.length() - 1);
    }
}
