/*
1. Определить, сколько раз повторяется в тексте каждое слово, которое встречается в нем.
2. В каждом слове текста k-ю букву заменить заданным символом. Если k больше длины слова, корректировку не выполнять.
3. Напечатать без повторения слова текста, у которых первая и последняя буквы совпадают
4. Подсчитать количество содержащихся в данном тексте знаков препинания
 */

package hw16;

import java.util.Map;

public class Runner {
    public static void main(String[] args) {
        WorkWithTextByPattern workWithTextByPattern = new WorkWithTextByPattern();
        System.out.println("Words and its numbers from text: ");
        for (Map.Entry<String, Integer> entry : workWithTextByPattern.wordAndItsNumber().entrySet()) {
            System.out.print(entry.getKey() + ": " + entry.getValue() + "; ");
        }
        System.out.println();
        System.out.println("There are " + workWithTextByPattern.punctuationMarksNumber() + " punctuation marks in the text.");
        System.out.println("Text after replacement third letter in each word from text: ");
        System.out.println(workWithTextByPattern.replaceSpecificLetterInWord());
        System.out.println("Words from text with equals first and last letters: ");
        for (String string : workWithTextByPattern.wordsWithEqualsFirstAndLastLetters()) {
            System.out.print(string + " ");
        }
    }
}
