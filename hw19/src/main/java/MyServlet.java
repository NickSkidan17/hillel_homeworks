import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

@WebServlet("/myservlet")
public class MyServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html><head>");
        out.println("<title>Sorting array of int nums array</title></head><body>");
        out.println("<h2>Input digits divided by coma:</h2>");
        out.println("<form method=\"post\" action=\"" + request.getContextPath() + "/myservlet\">");
        out.println("<table border=\"0\"><tr><td align=\"top\">");
        out.println("Your array:</td> <td valign=\"top\">");
        out.println("<input type=\"text\" name=\"Array for sorting\" size=\"20\">");
        out.println("</td></tr>");
        out.println("<tr><td valign=\"top\">");
        out.println("<input type=\"submit\" value=\"Send\"></td></tr>");
        out.println("</table></form>");
        out.println("</body></html>");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ArrayList<Integer> nums = new ArrayList<>();
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html><head>");
        out.println("<title>Sorted array</title></head><body>");
        out.println("<h2>This is sorted array:</h2>");
        String[] strArray = request.getParameter("Array for sorting").split(",");
        for (String str : strArray) {
            nums.add(Integer.valueOf(str));
        }
        Collections.sort(nums);
        String sortedArray = nums.toString();
        out.println("<strong>Result</strong> : " + sortedArray);
        out.println("<br />");
        out.println("</body></html>");
    }
}