/*
оздать консольное приложение, удовлетворяющее следующим требованиям:
• Использовать возможности ООП: классы, наследование, полиморфизм, инкапсуляция.
• Каждый класс должен иметь отражающее смысл название и информативный состав.
• Наследование должно применяться только тогда, когда это имеет смысл.
• При кодировании должны быть использованы соглашения об оформлении кода java code convention.
• Классы должны быть грамотно разложены по пакетам.
• Консольное меню должно быть минимальным.

Цветочница. Определить иерархию цветов. Создать несколько объектов-цветов.
Собрать букет (используя аксессуары) с определением его стоимости.
Провести сортировку цветов в букете на основе уровня свежести.
Найти цветок в букете, соответствующий заданному диапазону длин стеблей.

Создать свой Exception (унаследовать от Exception), бросать его в случае передачи методу по поиску
цветков по длинне отрицательного аргумента и соответственно обработать его в месте вызова этого метода
 */

package hw8;

import hw8.exception.NegativeStemLengthException;
import hw8.flowerbouquet.*;
import hw8.flowers.*;

public class Runner {
    public static void main(String[] args) {
        FlowerBouquetReader reader = new FlowerBouquetReader();
        reader.declareNumberOfFlowers();
        FlowerBouquetUtils flowerBouquet = new FlowerBouquetUtils(reader.fillBouquetWithFlowers(), reader.decorateBouquetWithAccessory());
        flowerBouquet.getFlowersBouquetCost();
        flowerBouquet.sortFlowersBouquetByFreshnessLevel();
        System.out.println(flowerBouquet);
        try {
            for (Flower flower : flowerBouquet.findFlowerByStemLength(reader.inputStemLengthRangeBoundary(), reader.inputStemLengthRangeBoundary())) {
                System.out.println(flower);
            }
        } catch (NegativeStemLengthException e) {
            System.out.println(e);
        }
    }
}
