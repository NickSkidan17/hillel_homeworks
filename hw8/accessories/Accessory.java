package hw8.accessories;

public class Accessory {
    private String accessoryName;
    private double price;

    public Accessory() {
    }

    public Accessory(String accessoryName, double price) {
        this.accessoryName = accessoryName;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Accessory " + accessoryName + "; price: " + price + " uah";
    }

    public String getAccessoryName() {
        return accessoryName;
    }

    public void setAccessoryName(String accessoryType) {
        this.accessoryName = accessoryName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}

