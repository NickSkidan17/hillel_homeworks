package hw8.flowers;

public class Orchid implements Flower {
    private String flowerName;
    private double price;
    private int freshnessLevel;
    private double stemLength;

    public Orchid(String flowerName, double price, int freshnessLevel, double stemLength) {
        this.flowerName = flowerName;
        this.price = price;
        this.freshnessLevel = freshnessLevel;
        this.stemLength = stemLength;
    }

    @Override
    public String toString() {
        return "Flower " + flowerName + "; price: " + price + " uah; freshness level: " + freshnessLevel + " days; stem length: " + stemLength + " inch";
    }

    @Override
    public String getFlowerName() {
        return flowerName;
    }

    @Override
    public void setFlowerName() {
        this.flowerName = flowerName;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public int getFreshnessLevel() {
        return freshnessLevel;
    }

    @Override
    public void setFreshnessLevel(int freshnessLevel) {
        this.freshnessLevel = freshnessLevel;
    }

    @Override
    public double getStemLength() {
        return stemLength;
    }

    @Override
    public void setStemLength(double stemLength) {
        this.stemLength = stemLength;
    }
}