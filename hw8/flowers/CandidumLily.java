package hw8.flowers;

public class CandidumLily extends Lily {
    public CandidumLily() {
        super("candidum lily", 25, 6, 19.8);
    }
}
