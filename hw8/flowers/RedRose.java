package hw8.flowers;

public class RedRose extends Rose {
    public RedRose() {
        super("red rose", 65, 1, 25);
    }
}
