package hw8.flowers;

public class CambriaOrchid extends Orchid {
    public CambriaOrchid() {
        super("cambria orchid", 90, 1, 22.4);
    }
}
