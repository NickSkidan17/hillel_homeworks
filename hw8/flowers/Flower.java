package hw8.flowers;

public interface Flower {

    String getFlowerName();

    void setFlowerName();

    double getPrice();

    void setPrice(double price);

    int getFreshnessLevel();

    void setFreshnessLevel(int freshnessLevel);

    double getStemLength();

    void setStemLength(double stemLength);
}
