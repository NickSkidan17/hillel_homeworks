package hw8.flowers;

public class BrassiaOrchid extends Orchid {
    public BrassiaOrchid() {
        super("brassia orchid", 55, 7, 9.3);
    }
}
