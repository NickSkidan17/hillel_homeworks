package hw8.flowers;

public class MartagonLily extends Lily {
    public MartagonLily() {
        super("martagon lily", 30, 5, 14.8);
    }
}
