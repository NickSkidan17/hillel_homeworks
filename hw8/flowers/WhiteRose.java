package hw8.flowers;

public class WhiteRose extends Rose {
    public WhiteRose() {
        super("white rose", 50, 4, 17.2);
    }
}
