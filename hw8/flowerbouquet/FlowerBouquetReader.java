package hw8.flowerbouquet;

import java.util.Scanner;

import hw8.accessories.*;
import hw8.exception.NegativeStemLengthException;
import hw8.flowers.*;

public class FlowerBouquetReader {
    private Flower[] flowers;
    private Accessory accessory;
    Scanner scanner = new Scanner(System.in);

    public void declareNumberOfFlowers() {
        int flowerLength;
        do {
            System.out.println("Input required number of flowers in a bouquet");
            while (!scanner.hasNextInt()) {
                System.out.println("It has to be a positive number");
                scanner.next();
            }
            flowerLength = scanner.nextInt();
        } while (flowerLength <= 0);
        flowers = new Flower[flowerLength];
    }

    public Flower[] fillBouquetWithFlowers() {
        System.out.println("List of flowers in stock: ");
        System.out.println("1 - brassia orchid;");
        System.out.println("2 - cambria orchid;");
        System.out.println("3 - candidum lily;");
        System.out.println("4 - martagon lily;");
        System.out.println("5 - red rose;");
        System.out.println("6 - white rose;");
        int chosenFlower = 0;
        do {
            System.out.println("Choose flowers in a bouquet (from 1 to 6): ");
            m:
            for (int i = 0; i < flowers.length; i++) {
                while (!scanner.hasNextInt()) {
                    System.out.println("It has to be a digit between 1 and 6");
                    scanner.next();
                }
                chosenFlower = scanner.nextInt();
                if (chosenFlower < 1 || chosenFlower > 6) {
                    System.out.println("Such flower is absent. Try again from the beginning");
                }
                switch (chosenFlower) {
                    case 1:
                        flowers[i] = new BrassiaOrchid();
                        break;
                    case 2:
                        flowers[i] = new CambriaOrchid();
                        break;
                    case 3:
                        flowers[i] = new CandidumLily();
                        break;
                    case 4:
                        flowers[i] = new MartagonLily();
                        break;
                    case 5:
                        flowers[i] = new RedRose();
                        break;
                    case 6:
                        flowers[i] = new WhiteRose();
                        break;
                    default:
                        break m;
                }
            }
        } while (chosenFlower < 1 || chosenFlower > 6);
        return flowers;
    }

    public Accessory decorateBouquetWithAccessory() {
        System.out.println("List of accessories in stock: ");
        System.out.println("0 - no accessory");
        System.out.println("1 - artificial mesh;");
        System.out.println("2 - pack paper;");
        System.out.println("3 - ribbon;");
        int chosenAccessory;
        do {
            System.out.println("Choose accessory to decorate a bouquet (from 0 to 3): ");
            while (!scanner.hasNextInt()) {
                System.out.println("It has to be a digit between 0 and 3");
                scanner.next();
            }
            chosenAccessory = scanner.nextInt();
            if (chosenAccessory < 0 || chosenAccessory > 3) {
                System.out.println("Such accessory is absent. Try again");
            }
            switch (chosenAccessory) {
                case 0:
                    accessory = new Accessory("is unneeded", 0);
                    break;
                case 1:
                    accessory = new ArtificialMesh();
                    break;
                case 2:
                    accessory = new PackPaper();
                    break;
                case 3:
                    accessory = new Ribbon();
                    break;
            }
        } while (chosenAccessory < 0 || chosenAccessory > 3);
        return accessory;
    }

    public double inputStemLengthRangeBoundary() throws NegativeStemLengthException {
        System.out.println("Input stem length range boundary: ");
        double stemLength = scanner.nextDouble();
        if (stemLength < 0) {
            throw new NegativeStemLengthException(stemLength);
        }
        return stemLength;
    }

    public Flower[] getFlowers() {
        return flowers;
    }

    public void setFlowers(Flower[] flowers) {
        this.flowers = flowers;
    }

    public Accessory getAccessory() {
        return accessory;
    }

    public void setAccessory(Accessory accessory) {
        this.accessory = accessory;
    }
}
