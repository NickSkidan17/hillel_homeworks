package hw8.flowerbouquet;

import hw8.accessories.Accessory;
import hw8.flowers.*;

public class FlowerBouquetUtils {
    private Flower[] flowers;
    private Accessory accessory;

    public FlowerBouquetUtils(Flower[] flowers, Accessory accessory) {
        this.flowers = flowers;
        this.accessory = accessory;
    }

    public void getFlowersBouquetCost() {
        double flowersBouquetPrice = 0;
        for (Flower flower : flowers) {
            flowersBouquetPrice += flower.getPrice();
        }
        System.out.println("\nTotal cost of your bouquet is " + (flowersBouquetPrice + accessory.getPrice()) + " uah\n");
    }

    public void sortFlowersBouquetByFreshnessLevel() {
        for (int i = 0; i < flowers.length; i++) {
            for (int j = i + 1; j < flowers.length; j++) {
                if (flowers[j].getFreshnessLevel() < flowers[i].getFreshnessLevel()) {
                    Flower tempFlower = flowers[i];
                    flowers[i] = flowers[j];
                    flowers[j] = tempFlower;
                }
            }
        }
    }

    public Flower[] findFlowerByStemLength(double minStemLength, double maxStemLength) {
        if (maxStemLength < minStemLength) {
            double tempVar = minStemLength;
            minStemLength = maxStemLength;
            maxStemLength = tempVar;
        }
        int counter = 0;
        Flower[] flowersWithRightStemLength = new Flower[0];
        for (Flower flower : flowers) {
            if (flower.getStemLength() >= minStemLength && flower.getStemLength() <= maxStemLength) {
                Flower[] tempArray = new Flower[flowersWithRightStemLength.length + 1];
                for (int i = 0; i < flowersWithRightStemLength.length; i++) {
                    tempArray[i] = flowersWithRightStemLength[i];
                }
                flowersWithRightStemLength = tempArray;
                tempArray[counter] = flower;
                counter++;
            }
        }
        return flowersWithRightStemLength;
    }

    @Override
    public String toString() {
        String result = "";
        for (Flower flower : flowers) {
            result += flower.toString() + "\n";
        }
        return result + accessory.toString() + "\n";
    }

    public Flower[] getFlowers() {
        return flowers;
    }

    public void setFlowers(Flower[] flowers) {
        this.flowers = flowers;
    }

    public Accessory getAccessory() {
        return accessory;
    }

    public void setAccessory(Accessory accessory) {
        this.accessory = accessory;
    }
}
