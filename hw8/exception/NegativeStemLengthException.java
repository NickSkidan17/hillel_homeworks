package hw8.exception;

public class NegativeStemLengthException extends Exception {
    private double stemLength;

    public NegativeStemLengthException(double stemLength) {
        this.stemLength = stemLength;
    }

    public String toString() {
        return "Stem length can't be negative. Input correct data. ";
    }
}