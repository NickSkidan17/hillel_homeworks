package hw14.task4;

import java.util.HashSet;
import java.util.Set;

public class Task4 {

    public Set union(Set set1, Set set2) {
        Set<Integer> result = new HashSet<>();
        result.addAll(set1);
        result.addAll(set2);
        return result;
    }

    public Set intersect(Set set1, Set set2) {
        set1.retainAll(set2);
        return set1;
    }
}
