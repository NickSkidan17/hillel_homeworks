//Определить множество на основе множества целых чисел. Создать методы для определения пересечения и объединения множеств

package hw14.task4;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Runner {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5));
        Set<Integer> set2 = new HashSet<>(Arrays.asList(4, 5, 6, 7, 8));
        Task4 task = new Task4();
        System.out.printf("union: %s%n", task.union(set1, set2));
        System.out.printf("intersect: %s%n", task.intersect(set1, set2));
    }
}
