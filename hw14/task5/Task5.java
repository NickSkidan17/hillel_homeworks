//Ввести строки из файла, записать в структуру данных (какую лучше, подумайте сами). Вывести строки в файл в обратном порядке

package hw14.task5;

import java.io.*;
import java.util.ArrayDeque;
import java.util.Scanner;

public class Task5 {

    public ArrayDeque<String> fileToDeque(File file) {
        ArrayDeque<String> deque = new ArrayDeque<>();
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNext()) {
                deque.addFirst(scanner.nextLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deque;
    }

    public void dequeToFile(ArrayDeque<String> deque) {
        try (FileWriter fw = new FileWriter(new File("C:\\JavaStudy\\JEStudy\\src\\hw14\\txtfiles\\test1.txt"))) {
            for (String string : deque) {
                fw.write(string + "\r\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
