/*
Задан файл с текстом на английском языке. Выделить все различные слова.
   Для каждого слова подсчитать частоту его встречаемости. Слова, отличаю-
   щиеся регистром букв, считать различными. Использовать класс HashMap
 */

package hw14.task3;

import java.io.File;

public class Runner {

    private static final String FILE_IN = "C:\\JavaStudy\\JEStudy\\src\\hw14\\txtfiles\\test.txt";

    public static void main(String[] args) {
        Task3 task = new Task3();
        task.print(task.listToMap(task.fileToList(new File(FILE_IN))));
    }
}
