package hw14.task3;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Task3 {

    public ArrayList<String> fileToList(File file) {
        ArrayList<String> list = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                list.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Map<String, Integer> listToMap(ArrayList<String> list) {
        Map<String, Integer> map = new HashMap<>();
        list.forEach(line -> {
            String words[] = line.split(" ");
            for (String word : words) {
                if (map.containsKey(word)) {
                    map.put(word, map.get(word) + 1);
                    continue;
                }
                map.put(word, 1);
            }
        });
        return map;
    }

    public void print(Map<String, Integer> map) {
        System.out.println("Unique words: ");
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 1) {
                System.out.print(entry.getKey() + " : " + entry.getValue() + "; ");
            }
        }
        System.out.println();
        System.out.println("Repeated words: ");
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue() != 1) {
                System.out.print(entry.getKey() + " : " + entry.getValue() + "; ");
            }
        }
    }
}
