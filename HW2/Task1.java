//Объявить массив с n-м количеством случайных чисел (явно инициализировать массив) и выбрать из него четные и нечетные числа

package hw2;

public class Task1 {
    public static void main(String[] args) {
        int[] nums = {6, 17, 8, 154, 90, 55, 91, 104, 13};
        System.out.print("Even numbers from array: ");
        for (int i = 0; i < nums.length; i++) {
            if ((nums[i] % 2) == 0) {
                System.out.print(nums[i] + " ");
            }
        }

        System.out.println();

        System.out.print("Odd numbers from array: ");
        for (int i = 0; i < nums.length; i++) {
            if ((nums[i] % 2) != 0) {
                System.out.print(nums[i] + " ");
            }
        }
    }
}
