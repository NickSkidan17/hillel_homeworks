/*
Создайте массив из 20-ти первых чисел Фибоначчи и выведите его на экран
(первый и второй члены последовательности равны единицам, а каждый следующий — сумме двух предыдущих)
*/

package hw2;

public class Task3 {
    public static void main(String[] args) {
        int fibNums[] = new int[20];
        fibNums[0] = fibNums[1] = 1;
        for (int i = 2; i < fibNums.length; i++) {
            fibNums[i] = fibNums[i - 1] + fibNums[i - 2];
        }
        System.out.print(" Fibonacci sequence's first twenty numbers: ");
        for (int i = 0; i < fibNums.length; i++) {
            System.out.print(fibNums[i] + " ");
        }
    }
}
