//Объявить массив с n-м количеством случайных чисел (явно инициализировать массив) и выбрать из него наибольшее и наименьшее число

package hw2;

public class Task4 {
    public static void main(String[] args) {
        int[] nums = {1, -234, -300, 45, 67, 290, 3005, 20987, -7380};
        int max = nums[0];
        int min = nums[0];
        for (int i : nums) {
            if (i >= max) {
                max = i;
            }
            if (i <= min) {
                min = i;
            }
        }
        System.out.println("The smallest number from array is " + min);
        System.out.println("The largest number from array is " + max);
    }
}
