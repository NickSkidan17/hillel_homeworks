//Объявить массив с n-м количеством случайных чисел (явно инициализировать массив) и выбрать из него простые числа

package hw2;

public class Task2 {
    public static void main(String[] args) {
        boolean isPrime;
        int[] nums = {1, -86, 5, 65, 97, 190, 131, -3, 239, 13, 0, 17, 3, 156, 93, 2};
        for (int i : nums) {
            isPrime = true;
            for (int j = 2; j < i / 2; j++) {
                if (i % j == 0) {
                    isPrime = false;
                }
            }
            if (isPrime & (i > 1)) {
                System.out.println("Reader " + i + " is prime.");
            }
        }
    }
}