package hw9;

import java.util.ArrayList;

public class PrimeNumberThread extends Thread {
    private ArrayList<Integer> primeNumbers = new ArrayList<>();
    private int[] numbers;
    private Object object;

    PrimeNumberThread(int[] numbers, Object object) {
        this.numbers = numbers;
        this.object = object;
    }

    public void run() {
        synchronized (object) {
            boolean isPrime;
            for (int i : numbers) {
                isPrime = true;
                for (int j = 2; j <= i / 2; j++) {
                    if (i % j == 0) {
                        isPrime = false;
                    }
                }
                if (isPrime && i != 1) {
                    primeNumbers.add(i);
                }
            }
            for (int k : primeNumbers) {
                System.out.print(k + " ");
            }
            System.out.print("/ ");
        }

    }

    public ArrayList<Integer> getPrimeNumbers() {
        return primeNumbers;
    }

    public void setPrimeNumbers(ArrayList<Integer> primeNumbers) {
        this.primeNumbers = primeNumbers;
    }

    public int[] getNumbers() {
        return numbers;
    }

    public void setNumbers(int[] numbers) {
        this.numbers = numbers;
    }
}