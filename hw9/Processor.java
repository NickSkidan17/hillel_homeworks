package hw9;

import java.util.Arrays;
import java.util.Scanner;

public class Processor {
    Scanner scanner = new Scanner(System.in);
    private int[] numbers;
    private int numbersLength;
    private PrimeNumberThread[] primeNumberThreads;

    public int[] declareNumbersRange() {
        System.out.println("Input lower bound of number's range: ");
        int lowerBound = scanner.nextInt();
        System.out.println("Input upper bound of number's range: ");
        int upperBound = scanner.nextInt();
        numbersLength = upperBound - lowerBound + 1;
        numbers = new int[numbersLength];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = lowerBound;
            lowerBound++;
        }
        return numbers;
    }

    public void declareThreadsNumber() {
        System.out.println("Input needed thread's number: ");
        int threadsLength = scanner.nextInt();
        primeNumberThreads = new PrimeNumberThread[threadsLength];
        Object object = new Object();
        int step = numbersLength / threadsLength;
        for (int i = 0; i < threadsLength; i++) {
            int indexFrom = step * i;
            int indexTo = step * (i + 1);
            if (i == threadsLength - 1) {
                indexTo = numbers.length;
            }
            int[] numbersInCurrentThread = Arrays.copyOfRange(numbers, indexFrom, indexTo);
            primeNumberThreads[i] = new PrimeNumberThread(numbersInCurrentThread, object);
            primeNumberThreads[i].start();
        }
        try {
            for (int i = 0; i < threadsLength; i++) {
                primeNumberThreads[i].join();
            }
        } catch (InterruptedException e) {

        }
    }

    public int[] getNumbers() {
        return numbers;
    }

    public void setNumbers(int[] numbers) {
        this.numbers = numbers;
    }

    public int getNumbersLength() {
        return numbersLength;
    }

    public void setNumbersLength(int numbersLength) {
        this.numbersLength = numbersLength;
    }

    public PrimeNumberThread[] getPrimeNumberThreads() {
        return primeNumberThreads;
    }

    public void setPrimeNumberThreads(PrimeNumberThread[] primeNumberThreads) {
        this.primeNumberThreads = primeNumberThreads;
    }
}
