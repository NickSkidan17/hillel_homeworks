/*
Реализовать инициализацию массива Car'ов (из задания про таксопарк) на основе файла (машины должны храниться в файле).
Структура файла:

VAZ,2104,15,180,1000,universal;
VAZ,21099,12,200,1500,sedan

Соотвутствующие столбцы:
mark,model,fuel,speed,price,тип_создаваемого_объекта;

Запятая (,) - разделитель свойств машины
Точка с запятой и конец строки (;\n\r) - разделитель машин

Выполнить чтение всех байтов с файла с помощью FileInputStream в массив байтов.
Создать строку на основе прочитанного массива байтов (String.trim).
Разделить полученную строку таким образом, чтобы создать экземпляры классов(String.split).

Так же, реализовать запись в файл в соответствующем формате
 */

package hw10;

import hw10.entities.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;

import static java.lang.Integer.parseInt;

public class Runner {
    private static final String FILE_IN_NAME = "C:\\JavaStudy\\JEStudy\\src\\hw10\\txtfiles\\cars.txt";
    private static final String FILE_OUT_NAME = "C:\\JavaStudy\\JEStudy\\src\\hw10\\txtfiles\\cars1.txt";

    public static void main(String[] args) throws Exception {
        File file = new File(FILE_IN_NAME);
        FileInputStream fin = new FileInputStream(FILE_IN_NAME);
        byte[] byteCars = new byte[(int) file.length()];
        fin.read(byteCars);
        fin.close();
        String textCars = new String(byteCars).trim();
        String[] stringCars = textCars.split(";");
        Car[] cars = new Car[stringCars.length];
        for (int i = 0; i < stringCars.length; i++) {
            String[] carProperties = stringCars[i].split(",");
            switch (carProperties[carProperties.length - 1]) {
                case "sedan": {
                    cars[i] = new Sedan(carProperties[0], carProperties[1], parseInt(carProperties[2]), parseInt(carProperties[3]), parseInt(carProperties[4]));
                }
                break;
                case "universal": {
                    cars[i] = new Universal(carProperties[0], carProperties[1], parseInt(carProperties[2]), parseInt(carProperties[3]), parseInt(carProperties[4]));
                }
                break;
                case "jeep": {
                    cars[i] = new Jeep(carProperties[0], carProperties[1], parseInt(carProperties[2]), parseInt(carProperties[3]), parseInt(carProperties[4]));
                }
                break;
            }
        }
        FileOutputStream fout = new FileOutputStream(FILE_OUT_NAME);
        System.setOut(new PrintStream(fout));
        for (Car car : cars) {
            System.out.print(car);
        }
        fout.close();
    }
}
