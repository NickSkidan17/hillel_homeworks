package hw10.entities;

public class Sedan extends Car {
    public Sedan(String mark, String model, int fuelConsumption, int speed, int price) {
        super(mark, model, fuelConsumption, speed, price);
    }

    @Override
    public String toString() {
        return super.toString() + "sedan;";
    }
}
