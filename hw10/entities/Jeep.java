package hw10.entities;

public class Jeep extends Car {
    public Jeep(String mark, String model, int fuelConsumption, int speed, int price) {
        super(mark, model, fuelConsumption, speed, price);
    }

    @Override
    public String toString() {
        return super.toString() + "jeep;";
    }
}
