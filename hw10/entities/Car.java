package hw10.entities;

public class Car {
    private String mark;
    private String model;
    private int fuelConsumption;
    private int speed;
    private int price;

    Car(String mark, String model, int fuelConsumption, int speed, int price) {
        this.mark = mark;
        this.model = model;
        this.fuelConsumption = fuelConsumption;
        this.speed = speed;
        this.price = price;
    }

    @Override
    public String toString() {
        return mark + "," + model + "," + fuelConsumption + "," + speed + "," + price + ",";
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}