package com.work.nickskidan.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.work.nickskidan.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {
}
