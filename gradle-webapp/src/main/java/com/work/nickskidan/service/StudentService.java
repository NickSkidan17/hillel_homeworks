package com.work.nickskidan.service;

import com.work.nickskidan.entity.Student;

public interface StudentService {
    Student findStudentById(Integer id);
}
