package com.work.nickskidan.service.impl;

import com.work.nickskidan.entity.Student;
import com.work.nickskidan.repository.StudentRepository;
import com.work.nickskidan.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    StudentRepository studentRepository;

    @Override
    public Student findStudentById(Integer id) {
        return studentRepository.findById(id).orElse(null);
    }
}