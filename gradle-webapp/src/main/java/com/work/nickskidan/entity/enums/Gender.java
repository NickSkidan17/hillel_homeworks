package com.work.nickskidan.entity.enums;

public enum Gender {
    MALE, FEMALE
}
