package com.work.nickskidan.controller;

import com.work.nickskidan.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {

    /*@GetMapping("")
    public String sayHello() {
        return "hello_world";
    }*/
    @Autowired
    StudentService studentService;

    @RequestMapping("/")
    @ResponseBody
    public String test() {
        return studentService.findStudentById(1).getName();
    }
}