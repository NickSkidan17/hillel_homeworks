package servlets;

import utils.Processor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/deletingFilms")
public class DeletingFilms extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("jsp/deletingfilms.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int deletedRows = 0;
        String parameter = request.getParameter("yearsQuantity");
        try {
            Processor processor = new Processor();
            deletedRows = processor.deletingFilmsReleasedOverNYearsAgo(Integer.valueOf(parameter));
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.setAttribute("deletedRows", deletedRows);
        request.getRequestDispatcher("jsp/deletedfilms.jsp").forward(request, response);
    }
}