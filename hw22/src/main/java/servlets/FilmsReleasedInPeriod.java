package servlets;

import entities.*;
import utils.Processor;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

@WebServlet("/filmsReleasedInPeriod")
public class FilmsReleasedInPeriod extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("jsp/filmsreleasedinperiod.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<Film> listFilms = new ArrayList<>();
        String paramYearFrom = request.getParameter("yearFrom");
        String paramYearTo = request.getParameter("yearTo");
        try {
            Processor processor = new Processor();
            listFilms = processor.getFilmsReleasedInGivenPeriod(Integer.valueOf(paramYearFrom), Integer.valueOf(paramYearTo));
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.setAttribute("listFilms", listFilms);
        request.getRequestDispatcher("jsp/filmslist.jsp").forward(request, response);
    }
}