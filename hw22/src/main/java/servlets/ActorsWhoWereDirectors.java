package servlets;

import entities.Actor;
import utils.Processor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/actorsWhoWereDirectors")
public class ActorsWhoWereDirectors extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<Actor> listActors = new ArrayList<>();
        try {
            Processor processor = new Processor();
            listActors = processor.getActorsWhoWereDirectors();
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.setAttribute("listActors", listActors);
        request.getRequestDispatcher("jsp/actorslist.jsp").forward(request, response);
    }
}
