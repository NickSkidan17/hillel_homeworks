package servlets;

import entities.User;
import utils.Processor;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class Login extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("jsp/login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = null;
        String login = request.getParameter("Login");
        String password = request.getParameter("Password");
        try {
            Processor processor = new Processor();
            user = processor.findUser(login, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (user == null) {
            String errorMessage = "Invalid login or password";
            request.setAttribute("errorMessage", errorMessage);
            request.getRequestDispatcher("jsp/login.jsp").forward(request, response);
        } else {
            session.setAttribute("user", user);
            RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/jsp/userinfo.jsp");
            dispatcher.forward(request, response);
        }
    }
}
