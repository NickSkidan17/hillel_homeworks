package servlets;

import entities.*;
import utils.Processor;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

@WebServlet("/actorsFromSeveralFilms")
public class ActorsFromSeveralFilms extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("jsp/actorsfromseveralfilms.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<Actor> listActors = new ArrayList<>();
        String parameter = request.getParameter("filmQuantity");
        try {
            Processor processor = new Processor();
            listActors = processor.getActorsPlayedInMoreThanNFilm(Integer.valueOf(parameter));
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.setAttribute("listActors", listActors);
        request.getRequestDispatcher("jsp/actorslist.jsp").forward(request, response);
    }
}