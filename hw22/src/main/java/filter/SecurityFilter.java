package filter;

import entities.User;
import org.apache.commons.lang.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SecurityFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        String uri = req.getRequestURI();
        String path = StringUtils.substringAfterLast(uri, "/");
        if (!path.equals("deletingFilms")) {
            chain.doFilter(request, response);
            return;
        }
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        if (user != null && user.getRole().equals("admin")) {
            chain.doFilter(request, response);
            return;
        }
        RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/jsp/accessdenied.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    public void destroy() {
    }

}
