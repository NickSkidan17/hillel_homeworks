<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locales.text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
<title>Actors from given film</title>
</head>
	<c:import url="../jspf/head.jspf"></c:import>
	<body>
	<center>
	    <br /><br />
    	<font size="+1" color="red"><td>${errorMessage}</font></td>
		<h5><fmt:message key="actorsfromfilm.message" /> : </h5>
		<form name="form1" method="post" action="http://localhost:8080/actorsFromFilm">
		<table border="0"><tr><td align="top">
		<label for="FilmTitle"><fmt:message key="actorsfromfilm.label.filmtitle" />:</label>
		<input type="text" name="FilmTitle" size="20">
		</td></tr>
		<tr><td align="top">
		<fmt:message key="actorsfromfilm.button.submit" var="buttonValue" />
        <input type="submit" name="submit" value="${buttonValue}"></td></tr>
		</table></form>
		</center>
	</body>
</html>