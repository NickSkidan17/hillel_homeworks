<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locales.text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
<title>Films released in given period</title>
</head>
	<c:import url="../jspf/head.jspf"></c:import>
	<body>
	<center>
		<h5><fmt:message key="filmsreleasedinperiod.message" /> : </h5>
		<form name="form1" method="post" action="http://localhost:8080/filmsReleasedInPeriod">
		<table border="0"><tr><td align="top">
		<label for="yearFrom"><fmt:message key="filmsreleasedinperiod.label.yearFrom" />:</label>
		</td> <td align="top">
		<input type="number" name="yearFrom" size="4" min="1900" max="2050" step="1">
		</td></tr>
		<tr><td valign="top">
		<label for="yearTo"><fmt:message key="filmsreleasedinperiod.label.yearTo" />:</label>
		</td> <td align="top">
		<input type="number" name="yearTo" size="4" min="1900" max="2050" step="1">
		</td></tr>
		<tr><td align="top">
		<fmt:message key="filmsreleasedinperiod.button.submit" var="buttonValue" />
        <input type="submit" name="submit" value="${buttonValue}"></td></tr>
		</table></form>
		</center>
	</body>
</html>