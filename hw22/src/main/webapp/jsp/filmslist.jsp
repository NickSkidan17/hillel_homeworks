<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locales.text" />
<!DOCTYPE html>
<html lang="${language}">
	<c:import url="../jspf/head.jspf"></c:import>
	<body>
	<center>
		<h5><fmt:message key="filmslist.message" /> : </h5>
		<c:forEach var="listFilms" items="${listFilms}">
			<tr>
			<font size="+2" color="indigo"<td>${listFilms.title};</td></font>
			<td>${listFilms.releaseYear};</td>
			<td>${listFilms.releaseCountry};</td>
			<font color="purple"><td><fmt:message key="filmslist.actors" />: ${listFilms.actors}</td></font>
			<font color="magenta"><td><fmt:message key="filmslist.director" />: ${listFilms.directors}</td></font>
			<br />
			</tr>
		</c:forEach>
		</center>
	</body>
</html>