<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locales.text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
<title>User Info</title>
</head>
	<c:import url="../jspf/head.jspf"></c:import>
	<body>
	<center>
		<h5><fmt:message key="userinfo.title" /> : </h5>
		<tr>
          	<fmt:message key="userinfo.hello" />, <font size="+2" color="navy"><td>${user.login}</font>!</td>
          	<br />
          	<fmt:message key="userinfo.role" />: <font size="+1" color="lime"><td>${user.role}</font></td>
        </tr>
		</center>
	</body>
</html>