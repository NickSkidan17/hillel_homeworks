<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locales.text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
<title>Deleting films</title>
</head>
	<c:import url="../jspf/head.jspf"></c:import>
	<body>
	<center>
		<h5><fmt:message key="deletingfilms.message" /> : </h5>
		<form name="form1" method="post" action="http://localhost:8080/deletingFilms">
		<table border="0"><tr><td align="top">
		<label for="yearsQuantity"><fmt:message key="deletingfilms.label.yearsQuantity" />:</label>
		<input type="number" name="yearsQuantity" size="3" min="1" max="100" step="1">
		</td></tr>
		<tr><td align="top">
		<fmt:message key="deletingfilms.button.submit" var="buttonValue" />
        <input type="submit" name="submit" value="${buttonValue}"></td></tr>
		</table></form>
		</center>
	</body>
</html>