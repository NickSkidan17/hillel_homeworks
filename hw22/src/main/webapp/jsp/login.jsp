<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locales.text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
<title>Login</title>
</head>
	<c:import url="../jspf/head.jspf"></c:import>
	<body>
	<center>
	    <br /><br />
	    <font size="+1" color="red"><td>${errorMessage}</font></td>
		<form name="form1" method="post" action="http://localhost:8080/login">
		<table border="0">
		<tr><td align="top">
		<label for="Login"><fmt:message key="login.label.login" />:</label>
		</td> <td align="top">
		<input type="text" name="Login">
		</td></tr>
		<tr><td align="top">
		<label for="Password"><fmt:message key="login.label.password" />:</label>
		</td> <td align="top">
		<input type="password" name="Password">
		</td></tr>
		<tr><td align="top">
		<fmt:message key="login.button.submit" var="buttonValue" />
		<input type="submit" name="submit" value="${buttonValue}"></td></tr>
		</table></form>
		</center>
	</body>
</html>