<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locales.text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
<title>Actors from several films</title>
</head>
	<c:import url="../jspf/head.jspf"></c:import>
	<body>
	<center>
		<h5><fmt:message key="actorsfromseveralfilms.message" /> : </h5>
		<form name="form1" method="post" action="http://localhost:8080/actorsFromSeveralFilms">
		<table border="0"><tr><td align="top">
		<label for="filmQuantity"><fmt:message key="actorsfromseveralfilms.label.filmQuantity" />:</label>
		<input type="number" name="filmQuantity" size="3" min="1" max="100" step="1">
		</td></tr>
		<tr><td align="top">
		<fmt:message key="actorsfromseveralfilms.button.submit" var="buttonValue" />
        <input type="submit" name="submit" value="${buttonValue}"></td></tr>
		</table></form>
		</center>
	</body>
</html>