<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locales.text" />
<!DOCTYPE html>
<html lang="${language}">
	<c:import url="../jspf/head.jspf"></c:import>
	<body>
	<center>
		<h5><fmt:message key="actorslist.message" /> : </h5>
		<c:forEach var="listActors" items="${listActors}">
			<tr>
			<font size="+2" color="red"><td>${listActors.name}</td></font>
			<td>; <fmt:message key="actorslist.birthdate" />: ${listActors.birthdate}</td>
			<br />
			</tr>
		</c:forEach>
		</center>
	</body>
</html>