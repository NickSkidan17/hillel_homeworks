<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="application" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="locales.text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
<title>User Info</title>
</head>
	<c:import url="../jspf/head.jspf"></c:import>
	<body>
	<center>
		<br/><br/>
      <h3 style="color:red;"><fmt:message key="accessdenied.message" />!</h3>
        <br/>
        <fmt:message key="accessdenied.tip" />
		</center>
	</body>
</html>