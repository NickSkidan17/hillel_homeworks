//Задать массив с n чисел. Найти числа, состоящее только из различных цифр

package hw3;

public class Task3 {
    public static boolean isDifferentDigits(int num) {
        int digits[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        boolean isDifDig = true;
        int index;
        while (num != 0) {
            index = num % 10;
            num /= 10;
            digits[index] += 1;
        }
        for (int i = 0; i < digits.length; i++) {
            if (digits[i] > 1) {
                isDifDig = false;
                break;
            }
        }
        return isDifDig;
    }

    public static void main(String[] args) {
        int[] nums = {6796865, 601, 45902, 4674858, 22, 10, 5589985, 9764, 14578, 113235458, 39510, 12356053};
        for (int number : nums) {
            if (isDifferentDigits(number))
                System.out.println("Reader " + number + " consists of different digits.");
        }
    }
}
