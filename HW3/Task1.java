//Умножить две матрицы

package hw3;

public class Task1 {
    public static void main(String[] args) {
        int matrix1[][] = {{5, 1, 9}, {0, 3, -7}, {9, -4, 15}};
        int matrix2[][] = {{11, -2, 5}, {1, -16, 8}, {-20, 3, 17}};
        if (matrix1[0].length == matrix2.length)
            System.out.println("Matrices can be multiplied!");
        else
            System.out.println("Orders of matrices are different.");

        int result[][] = new int[matrix1.length][matrix2[0].length];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix2[0].length; j++) {
                for (int k = 0; k < result.length; k++) {
                    result[i][j] += matrix1[i][k] * matrix2[k][j];
                }
            }
        }
        System.out.println("Matrix result is: ");
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                System.out.print(result[i][j] + " ");
            }
            System.out.println();
        }
    }
}
