//Вывести числа от 1 до k в виде матрицы N x N слева направо и сверху вниз

package hw3;

public class Task4 {
    public static void main(String[] args) {
        int k = (int) (Math.random() * 100);
        int rowsLength, columnsLength, counter = 0;
        columnsLength = rowsLength = (int) Math.ceil(Math.sqrt(k));
        System.out.println("Square matrix of numbers from 1 to " + k + ":");
        for (int i = 0; i < rowsLength; i++) {
            for (int j = 0; j < columnsLength; j++) {
                counter++;
                if (counter <= k) {
                    System.out.printf("%3d", counter);
                } else {
                    System.out.printf("%3d", 0);
                }
            }
            System.out.println();
        }
    }
}
