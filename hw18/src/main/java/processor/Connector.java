package processor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Connector {
    public static Connection getConnection() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/cinema";
        Properties prop = new Properties();
        prop.put("user", "root");
        prop.put("password", "avenir1987");
        prop.put("useSSL", "false");
        prop.put("serverTimezone", "UTC");
        return DriverManager.getConnection(url, prop);
    }
}
