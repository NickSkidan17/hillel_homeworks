package processor;

import entities.*;

import java.sql.*;
import java.util.ArrayList;

class Helper {
    private Connection connect;

    Helper() throws SQLException {
        connect = Connector.getConnection();
    }

    ArrayList<Actor> getResultForSelectingActors(String string) {
        ArrayList<Actor> listActors = new ArrayList<>();
        Statement st = null;
        try {
            st = getStatement();
            ResultSet rs;
            try {
                rs = st.executeQuery(string);
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    Date birthdate = rs.getDate(3);
                    listActors.add(new Actor(id, name, birthdate));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            closeStatement(st);
        }
        return listActors.size() > 0 ? listActors : null;
    }

    <T extends Person> ArrayList<T> getArrayListOfPersonsFromFilm(int filmID, String string) {
        ArrayList<T> persons = new ArrayList<>();
        PreparedStatement pst = null;
        try {
            pst = getPreparedStatement(string);
            pst.setInt(1, filmID);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                Date birthdate = rs.getDate(3);
                persons.add((T) new Person(id, name, birthdate));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closePreparedStatement(pst);
        }
        return persons.size() > 0 ? persons : null;
    }

    Statement getStatement() {
        Statement st = null;
        try {
            st = connect.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return st;
    }

    void closeStatement(Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private PreparedStatement getPreparedStatement(String string) {
        PreparedStatement pst = null;
        try {
            pst = connect.prepareStatement(string);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pst;
    }

    private void closePreparedStatement(PreparedStatement pst) {
        if (pst != null) {
            try {
                pst.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
