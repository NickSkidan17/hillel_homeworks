package processor;

import entities.*;

import java.sql.*;
import java.util.ArrayList;

public class Processor {
    private Helper helper;

    public Processor() throws SQLException {
        helper = new Helper();
    }

    public ArrayList<Actor> getActorsFromFilm(String filmTitle) {
        return helper.getResultForSelectingActors("select * from actor where idactor in \n" +
                "(select actor_id from film_actors \n" +
                "inner join film on filma_id = idfilm where idfilm in\n" +
                "(select idfilm from film where title = '" + filmTitle + "'))");
    }

    public ArrayList<Actor> getActorsWhoWereDirectors() {
        return helper.getResultForSelectingActors("select a.idactor, a.name, a.birthdate from actor a inner join director d on a.name = d.name");
    }

    public ArrayList<Actor> getActorsPlayedInMoreThanNFilm(int filmsNumber) {
        return helper.getResultForSelectingActors("select a.idactor, a.name, a.birthdate from actor a \n" +
                "inner join film_actors fa on a.idactor = fa.actor_id \n" +
                "group by fa.actor_id having count(filma_id) >= " + filmsNumber + ";");
    }

    public ArrayList<Film> getFilmsReleasedInGivenPeriod(int yearFrom, int yearTo) {
        ArrayList<Film> listFilms = new ArrayList<>();
        Statement st = null;
        try {
            st = helper.getStatement();
            ResultSet rs;
            try {
                rs = st.executeQuery("select * from film where YEAR(release_year) between " + yearFrom + " and " + yearTo + ";");
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String title = rs.getString(2);
                    Date releaseYear = rs.getDate(3);
                    String releaseCountry = rs.getString(4);
                    ArrayList<Actor> actors = helper.getArrayListOfPersonsFromFilm(id, "{call select_actors_from_film(?)}");
                    ArrayList<Director> director = helper.getArrayListOfPersonsFromFilm(id, "{call select_director_from_film(?)}");
                    listFilms.add(new Film(id, title, releaseYear, releaseCountry, actors, director));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            helper.closeStatement(st);
        }
        return listFilms.size() > 0 ? listFilms : null;
    }

    public int deletingFilmsReleasedOverNYearsAgo(int yearsAgo) {
        int deletedRows = 0;
        Statement st = null;
        try {
            st = helper.getStatement();
            try {
                deletedRows = st.executeUpdate("delete from film where release_year <= subdate(curdate(), interval" + yearsAgo + " YEAR)");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            helper.closeStatement(st);
        }
        return deletedRows;
    }
}
