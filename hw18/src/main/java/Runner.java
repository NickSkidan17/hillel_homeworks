/*
Перенести исходники из последнего домашнего задания по JDBC под проект на Maven.
Выполнить main метод с помощью Maven (Exec Maven Plugin)
 */

import entities.*;
import processor.Processor;

import java.sql.SQLException;

public class Runner {
    public static void main(String[] args) {
        Processor processor;
        try {
            processor = new Processor();
            System.out.println("Films from cinema released in given period: ");
            for (Film film : processor.getFilmsReleasedInGivenPeriod(2010, 2015)) {
                System.out.println(film);
            }
            System.out.println("Actors from film are: ");
            for (Actor actor : processor.getActorsFromFilm("Avengers")) {
                System.out.println(actor);
            }
            System.out.println("Actors, who were directors at the same time in their films: ");
            for (Actor actor : processor.getActorsWhoWereDirectors()) {
                System.out.println(actor);
            }
            System.out.println("Actors, who played in more than N films from my cinema: ");
            for (Actor actor : processor.getActorsPlayedInMoreThanNFilm(2)) {
                System.out.println(actor);
            }
            System.out.println("Deleting films, which were released more than N years ago: ");
            //System.out.println(processor.deletingFilmsReleasedOverNYearsAgo(40) + " film(s) was(were) deleted from cinema");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}