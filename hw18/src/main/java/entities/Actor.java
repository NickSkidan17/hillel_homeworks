package entities;

import java.util.Date;

public class Actor extends Person {
    public Actor(int id, String name, Date birthdate) {
        super(id, name, birthdate);
    }
}
