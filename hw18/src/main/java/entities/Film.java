package entities;

import java.util.ArrayList;
import java.util.Date;

public class Film {
    private int id;
    private String title;
    private Date releaseYear;
    private String releaseCountry;
    private ArrayList<Actor> actors;

    private ArrayList<Director> directors;

    public Film(int id, String title, Date releaseYear, String releaseCountry, ArrayList<Actor> actors, ArrayList<Director> directors) {
        this.id = id;
        this.title = title;
        this.releaseYear = releaseYear;
        this.releaseCountry = releaseCountry;
        this.actors = actors;
        this.directors = directors;
    }

    @Override
    public String toString() {
        return id + ". " + "Film's title: " + title + "; year of release: " + releaseYear + "; release country: " + releaseCountry +
                "; actors: " + actors + "; directors: " + directors + ".";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Date releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getReleaseCountry() {
        return releaseCountry;
    }

    public void setReleaseCountry(String releaseCountry) {
        this.releaseCountry = releaseCountry;
    }

    public ArrayList<Actor> getActors() {
        return actors;
    }

    public void setActors(ArrayList<Actor> actors) {
        this.actors = actors;
    }

    public ArrayList<Director> getDirectors() {
        return directors;
    }

    public void setDirectors(ArrayList<Director> directors) {
        this.directors = directors;
    }
}
