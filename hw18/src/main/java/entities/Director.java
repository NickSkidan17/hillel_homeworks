package entities;

import java.util.Date;

public class Director extends Person {
    public Director(int id, String name, Date birthdate) {
        super(id, name, birthdate);
    }
}
