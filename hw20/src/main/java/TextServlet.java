import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.*;

@WebServlet("/textServlet")
public class TextServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h2>Input word:</h2>");
        out.println("<form method=\"post\" action=\"" + request.getContextPath() + "/textServlet\">");
        out.println("<table border=\"0\"><tr><td align=\"top\">");
        out.println("<input type=\"text\" name=\"WordForFinding\" size=\"20\">");
        out.println("</td></tr>");
        out.println("<tr><td valign=\"top\">");
        out.println("<input type=\"submit\"value=\"Find\"></td></tr>");
        out.println("</table></form>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        String parameter = request.getParameter("WordForFinding");
        out.println("<h2>Number of word encounters in the text:</h2>");
        String filename = "/WEB-INF/text.txt";
        ServletContext context = getServletContext();
        InputStream inputStream = context.getResourceAsStream(filename);
        String result = "";
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String str;
            while ((str = br.readLine()) != null) {
                result += str;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Pattern pattern = Pattern.compile(parameter, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(result);
        int counter = 0;
        while (matcher.find()) {
            counter++;
        }
        out.println("<strong>Word " + parameter + " occurs</strong> : " + counter + " time(s)");
        out.println("<br />");
        out.println("</body></html>");
    }
}
