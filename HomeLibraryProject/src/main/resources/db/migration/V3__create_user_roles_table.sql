CREATE TABLE user_roles (
   user_id int NOT NULL UNIQUE,
   role_id int NOT NULL UNIQUE,

   FOREIGN KEY (user_id) REFERENCES users (id),
   FOREIGN KEY (role_id) REFERENCES roles (id)
);