CREATE TABLE videos (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  video_name varchar(45) NOT NULL,
  video_type varchar(20) NOT NULL,
  video_data mediumblob NOT NULL
  );