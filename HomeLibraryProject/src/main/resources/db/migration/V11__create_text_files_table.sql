CREATE TABLE text_files (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  text_file_name varchar(45) NOT NULL,
  text_file_type varchar(20) NOT NULL,
  text_file_data mediumblob NOT NULL
  );