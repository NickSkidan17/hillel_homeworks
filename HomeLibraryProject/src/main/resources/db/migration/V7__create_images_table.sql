CREATE TABLE images (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  image_name varchar(45) NOT NULL,
  image_type varchar(20) NOT NULL,
  image_data mediumblob NOT NULL
  );