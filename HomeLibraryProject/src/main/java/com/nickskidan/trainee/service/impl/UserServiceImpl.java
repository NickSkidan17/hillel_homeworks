package com.nickskidan.trainee.service.impl;

import com.nickskidan.trainee.dao.RoleDao;
import com.nickskidan.trainee.dao.UserDao;
import com.nickskidan.trainee.model.Role;
import com.nickskidan.trainee.model.User;
import com.nickskidan.trainee.service.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    private RoleDao roleDao;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    UserServiceImpl(UserDao userDao, RoleDao roleDao, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDao = userDao;
        this.roleDao = roleDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    UserServiceImpl() {
    }

    @Override
    public void saveUser(User user) {
        Set<Role> roles = new HashSet<>();
        User userFromDB = userDao.findByLogin(user.getLogin());
        if (userFromDB == null) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            roles.add(roleDao.getOne(2));
            userDao.save(user);
        }
    }

    @Override
    public User findByLogin(String login) {
        return userDao.findByLogin(login);
    }

    @Override
    public User findByEmail(String email) {
        return userDao.findByEmail(email);
    }
}