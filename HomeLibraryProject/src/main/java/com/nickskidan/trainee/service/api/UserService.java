package com.nickskidan.trainee.service.api;

import com.nickskidan.trainee.model.User;

public interface UserService {
    void saveUser(User user);
    User findByLogin(String login);
    User findByEmail(String email);
}
