package com.nickskidan.trainee.service.api;

import com.nickskidan.trainee.model.TextFile;

import java.util.List;

public interface TextFileService{
    TextFile findById(Integer id);
    List<TextFile> findAll();
}
