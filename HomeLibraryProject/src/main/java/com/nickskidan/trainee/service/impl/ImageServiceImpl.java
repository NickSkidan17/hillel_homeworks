package com.nickskidan.trainee.service.impl;

import com.nickskidan.trainee.dao.ImageDao;
import com.nickskidan.trainee.model.Image;
import com.nickskidan.trainee.service.api.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImageServiceImpl implements ImageService {

    private ImageDao imageDao;

    @Autowired
    ImageServiceImpl(ImageDao imageDao) {
        this.imageDao = imageDao;
    }

    @Override
    public Image findById(Integer id) {
        return imageDao.findById(id).orElse(null);
    }

    @Override
    public List<Image> findAll() {
        return imageDao.findAll();
    }

}
