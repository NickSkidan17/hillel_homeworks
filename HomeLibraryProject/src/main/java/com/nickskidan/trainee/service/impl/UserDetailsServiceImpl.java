package com.nickskidan.trainee.service.impl;

import com.nickskidan.trainee.model.Role;
import com.nickskidan.trainee.model.User;
import com.nickskidan.trainee.service.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private UserService userService;

    @Autowired
    UserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    UserDetailsServiceImpl() {
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        User userFindByLogin = userService.findByLogin(login);
        User userFindByEmail = userService.findByEmail(login);

        if (userFindByLogin != null) {
            Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

            for (Role role : userFindByLogin.getRoles()) {
                grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
            }
            return new org.springframework.security.core.userdetails.User(userFindByLogin.getLogin(), userFindByLogin.getPassword(), grantedAuthorities);
        }

        if (userFindByEmail != null) {
            Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

            for (Role role : userFindByEmail.getRoles()) {
                grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
            }
            return new org.springframework.security.core.userdetails.User(userFindByEmail.getLogin(), userFindByEmail.getPassword(), grantedAuthorities);
        }

        return null;
    }
}
