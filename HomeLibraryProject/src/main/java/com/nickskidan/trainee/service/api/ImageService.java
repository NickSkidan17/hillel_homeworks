package com.nickskidan.trainee.service.api;

import com.nickskidan.trainee.model.Image;

import java.util.List;

public interface ImageService {
    Image findById(Integer id);
    List<Image> findAll();
}
