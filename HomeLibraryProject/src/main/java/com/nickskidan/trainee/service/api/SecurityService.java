package com.nickskidan.trainee.service.api;

public interface SecurityService {
    String findLoggedInLogin();
    void autoLogin(String login, String password);
}
