package com.nickskidan.trainee.service.impl;

import com.nickskidan.trainee.dao.VideoDao;
import com.nickskidan.trainee.model.Video;
import com.nickskidan.trainee.service.api.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {

    private VideoDao videoDao;

    @Autowired
    VideoServiceImpl(VideoDao videoDao) {
        this.videoDao = videoDao;
    }

    @Override
    public Video findById(int id) {
        return videoDao.findById(id).orElse(null);
    }

    @Override
    public List<Video> findAll() {
        return videoDao.findAll();
    }
}
