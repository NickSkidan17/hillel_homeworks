package com.nickskidan.trainee.service.impl;

import com.nickskidan.trainee.dao.TextFileDao;
import com.nickskidan.trainee.model.TextFile;
import com.nickskidan.trainee.service.api.TextFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TextFileServiceImpl implements TextFileService {

    private TextFileDao textFileDao;

    @Autowired
    TextFileServiceImpl(TextFileDao textFileDao) {
        this.textFileDao = textFileDao;
    }

    @Override
    public TextFile findById(Integer id) {
        return textFileDao.findById(id).orElse(null);
    }

    @Override
    public List<TextFile> findAll() {
        return textFileDao.findAll();
    }
}
