package com.nickskidan.trainee.service.api;

import com.nickskidan.trainee.model.Video;

import java.util.List;

public interface VideoService {
    Video findById(int id);
    List<Video> findAll();
}
