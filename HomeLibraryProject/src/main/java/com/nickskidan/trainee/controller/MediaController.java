package com.nickskidan.trainee.controller;

import com.nickskidan.trainee.model.Image;
import com.nickskidan.trainee.model.TextFile;
import com.nickskidan.trainee.model.Video;
import com.nickskidan.trainee.service.api.ImageService;
import com.nickskidan.trainee.service.api.TextFileService;
import com.nickskidan.trainee.service.api.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class MediaController {

    private ImageService imageService;
    private TextFileService textFileService;
    private VideoService videoService;

    @Autowired
    MediaController(ImageService imageService, TextFileService textFileService, VideoService videoService) {
        this.imageService = imageService;
        this.textFileService = textFileService;
        this.videoService = videoService;
    }

    @GetMapping("/images")
    public ModelAndView getImageList() {
        List<Image> imageList = imageService.findAll();
        ModelAndView mav = new ModelAndView("images");
        mav.addObject("imageList", imageList);
        return mav;
    }

    @GetMapping(value = "/view_image/{id}", produces = "image/jpeg")
    public @ResponseBody
    byte[] getImage(@PathVariable Integer id) {
        byte[] image = imageService.findById(id).getImageData();
        return image;
    }

    @GetMapping("/text_files")
    public ModelAndView getTextFilesList() {
        List<TextFile> textFilesList = textFileService.findAll();
        ModelAndView mv = new ModelAndView("text_files");
        mv.addObject("textFilesList", textFilesList);
        return mv;
    }

    @GetMapping(value = "/view_text_file/{id}", produces = "application/msword")
    public @ResponseBody
    byte[] getTextFile(@PathVariable Integer id) {
        byte[] textFile = textFileService.findById(id).getTextFileData();
        return textFile;
    }

    @GetMapping("/videos")
    public ModelAndView getVideosList() {
        List<Video> videosList = videoService.findAll();
        ModelAndView mv = new ModelAndView("videos");
        mv.addObject("videosList", videosList);
        return mv;
    }

    @GetMapping(value = "/view_video/{id}", produces = "video/mp4")
    public @ResponseBody
    byte[] getVideo(@PathVariable Integer id) {
        byte[] video = videoService.findById(id).getVideoData();
        return video;
    }

}
