package com.nickskidan.trainee.dao;

import com.nickskidan.trainee.model.TextFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TextFileDao extends JpaRepository<TextFile, Integer> {
}
