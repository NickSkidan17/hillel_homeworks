package com.nickskidan.trainee.dao;

import com.nickskidan.trainee.model.Video;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VideoDao extends JpaRepository<Video, Integer> {
}
