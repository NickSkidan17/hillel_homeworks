package com.nickskidan.trainee.dao;

import com.nickskidan.trainee.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User, Integer> {
    User findByLogin(String login);
    User findByEmail(String email);
}
