package com.nickskidan.trainee.dao;

import com.nickskidan.trainee.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageDao extends JpaRepository<Image, Integer> {
}
