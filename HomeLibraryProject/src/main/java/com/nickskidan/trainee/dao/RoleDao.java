package com.nickskidan.trainee.dao;

import com.nickskidan.trainee.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDao extends JpaRepository<Role, Integer> {
}
