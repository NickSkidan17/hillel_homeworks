package com.nickskidan.trainee.model;

import com.mysql.cj.jdbc.Blob;

import javax.persistence.*;

@Entity
@Table(name = "text_files")
public class TextFile {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "text_file_name")
    private String textFileName;

    @Column(name = "text_file_type")
    private String textFileType;

    @Column(name = "text_file_data")
    @Lob
    private byte[] textFileData;

    public TextFile() {
    }

    public TextFile(int id) {
        this.id = id;
    }

    public TextFile(int id, String textFileName, String textFileType, byte[] textFileData) {
        this.id = id;
        this.textFileName = textFileName;
        this.textFileType = textFileType;
        this.textFileData = textFileData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTextFileName() {
        return textFileName;
    }

    public void setTextFileName(String textFileName) {
        this.textFileName = textFileName;
    }

    public String getTextFileType() {
        return textFileType;
    }

    public void setTextFileType(String textFileType) {
        this.textFileType = textFileType;
    }

    public byte[] getTextFileData() {
        return textFileData;
    }

    public void setTextFileData(byte[] textFileData) {
        this.textFileData = textFileData;
    }

}
