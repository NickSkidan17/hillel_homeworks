package com.nickskidan.trainee.model;

import com.mysql.cj.jdbc.Blob;

import javax.persistence.*;

@Entity
@Table(name = "videos")
public class Video {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "video_name")
    private String videoName;

    @Column(name = "video_type")
    private String videoType;

    @Column(name = "video_data")
    @Lob
    private byte[] videoData;

    public Video() {
    }

    public Video(int id) {
        this.id = id;
    }

    public Video(int id, String videoName, String videoType, byte[] videoData) {
        this.id = id;
        this.videoName = videoName;
        this.videoType = videoType;
        this.videoData = videoData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoType() {
        return videoType;
    }

    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }

    public byte[] getVideoData() {
        return videoData;
    }

    public void setVideoData(byte[] videoData) {
        this.videoData = videoData;
    }

}